# [4.1.0](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/compare/v4.0.9...v4.1.0) (2024-05-23)


### Bug Fixes

* adapt some versions ([ccda2e2](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/ccda2e2fbdeda8f31e0efd0f83277367b2df94bf))
* adapt spring version to spring-elasticsearch ([172ef19](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/172ef199aa6c7385ea17ace4f79b95ba7dd9dd14))
* add correct jdk version ([5c85e78](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/5c85e7850d96316a11c45beec52b48641b952c7c))
* add pilato spring-elasticsearch again ([b5bf484](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/b5bf484bc385224a276be1f3b0457fda6ffcdcd2))
* add servlet dep ([be202b1](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/be202b19e5129b0c98e18e7a037834e28ebe4817))
* add tomcat 10 to dockerfile! :-D ...immer dasselbe ([b631144](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/b6311449ab380dba4ba353f5e231c9d3a95abe46))
* add transport ([7b0f006](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/7b0f00635937f2bcdef59d37b6bd072322bb84ee))
* add war plugin ([a1033bf](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/a1033bfee6bf0bca55cf43105204ddf37fa8d4bf))
* change exclusion to jsonld ([2370f7a](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/2370f7a1b05a64665d6695eae7ff5427be92d2d4))
* do we need spring-context? ([a56bdb2](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/a56bdb2f4caf6d516700b0749583c5cc20b31c59))
* doch transport? ([449af8a](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/449af8abdd1b09b316e5575267b06adad09d6e76))
* exclude httpclients from rdfutils ([4b5c9b5](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/4b5c9b58c9523b68f69c53b78154b5a2a7802902))
* fix dep configuration ([305851c](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/305851c0c659a84af7b7c6c351eef14e49521489))
* increase common to 5.0.0 release (es7) ([97a2f31](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/97a2f3194d6fc6f05b8833d751038b1d13f459bf))
* increase cxf and spring version ([572d04f](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/572d04f43d0de62416a959c2f50ac5fe987e8686))
* increase cxf version and related packages ([01f7f96](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/01f7f96d07f33ce633f6bcc3c47869a88d5ca8ca))
* increase dependency versions ([e0d171c](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/e0d171cdab86d97a1a64d7b9b3b2583d0efdea9f))
* increase jena ([137744e](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/137744e35f124204fab6c4e13edbd1737ab0e7ec))
* increase jetty version ([5c9d9bc](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/5c9d9bc14e5ca937b3f10ed89715f6abebe5badd))
* increase json versions ([83f52ae](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/83f52aea42076affc68605b9527ce6ad5f7ed96f))
* increase tgsearch version ([c6e9682](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/c6e9682396521d9a6de1b8a245753a40fc6e38ef))
* play with exclusions ([e3650aa](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/e3650aae71105627fb6e554b3a7965502ad3ac3f))
* re-set jsonld version... :-D ([4fc7fa6](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/4fc7fa6e40ab429d6ddec0335d12d0af7fe445cf))
* remove exclusions ([dfab283](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/dfab2838460bb938f1329d7965967d4d223569d5))
* remove unused deps and versions ([3c09d0e](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/3c09d0ecc597392fd04a515ee832bec82cacff8d))
* removing cxf-rt things?????????????? ([13128bd](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/13128bdf4dc02eb05b2f6af98ad5b4afbb5e7c5f))
* sort out dependencies ([3d73a76](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/3d73a76172ed8746e9e6ccc29db17bf5b7d640a7))
* still more http exclusions ([e6369f5](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/e6369f5c0338d51b46eb38778188f190cc2b7079))
* update spring-elasticsearch ([66a59d8](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/66a59d8a7bf108b1107da54fcb2b3b4ea3468295))
* update to es7, remove es type ([771204e](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/771204e0f65a0200fa941bc4199207e53a04b7e1))
* use jakarta servlet 6.0.0 ([3ba4ff8](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/3ba4ff8666a8d916396c5866a13658afd623bfa9))
* wildes rumtesten, wieso das nicht rennt ([31663d7](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/31663d70db6f96e8cec993c87316e1823e80d367))


### Features

* adapt dockerfile and gitlab ci and pom versions to Java 17 ([6069803](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/606980356e40f21a4643ae41c06a81e5fab3cb81))

## [4.0.9](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/compare/v4.0.8...v4.0.9) (2023-08-25)


### Bug Fixes

* do some cleaning ([a5ed271](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/a5ed2711dc93e55e4c3a60e6c1de70d5bce40f5d))
* fix config and presentation url in dhrep html template ([02faea0](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/02faea096458865157d01b02749ef18e50081a5a))
* fix index html tests ([38ce34f](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/38ce34f5e3d386085661ee5eeba702368336cb7d))

## [4.0.8](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/compare/v4.0.7...v4.0.8) (2023-08-22)


### Bug Fixes

* change tg.esNodes to esNodes in config for general elasticsearch usage ([d1ca235](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/d1ca235f1ea0e7c792bf820e49234c0cdd4e8519))

## [4.0.7](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/compare/v4.0.6...v4.0.7) (2023-08-21)


### Bug Fixes

* remove dry-run from semantic release workflow ([9de62d4](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/commit/9de62d41693744a2b43a7af944c1924fdc842d41))
