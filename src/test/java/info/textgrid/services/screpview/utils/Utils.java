package info.textgrid.services.screpview.utils;

import java.io.File;
import java.io.IOException;
import info.textgrid.middleware.common.CachingUtils;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class Utils {

  public static final File CACHE_DIR = new File("src/test/resources/cache/");
  public static final String IMAGE_SERVICE_CONTEXT_URL = "http://iiif.io/api/image/2/context.json";
  public static final String PRESENTATION_SERVICE_CONTEXT_URL =
      "http://iiif.io/api/presentation/2/context.json";

  /**
   * @param id
   * @throws IOException
   */
  public static void deleteFileAndFolder(String id) throws IOException {

    File fol = new File(CACHE_DIR, CachingUtils.getCachingPathFromURI(id)).getParentFile();
    File files[] = fol.listFiles();
    if (files != null) {
      // Delete files.
      for (File fil : fol.listFiles()) {
        boolean deleted = fil.delete();
        if (deleted) {
          System.out.println("file deleted: " + fil.getCanonicalPath());
        } else {
          System.out.println("file not deleted: " + fil.getCanonicalPath());
        }
      }
      // Delete folder.
      if (fol.listFiles().length == 0) {
        boolean deleted = fol.delete();
        if (deleted) {
          System.out.println("folder deleted: " + fol.getCanonicalPath());
        } else {
          System.out.println("folder not deleted: " + fol.getCanonicalPath());
        }
      }
    }
  }

}
