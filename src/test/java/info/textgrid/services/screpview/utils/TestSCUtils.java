package info.textgrid.services.screpview.utils;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;
import com.github.jsonldjava.core.JsonLdError;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class TestSCUtils {

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  /**
   * @throws IOException
   * @throws JsonLdError
   * @throws ParseException
   */
  @Test
  public void testTtl2jsonldDariah() throws IOException, JsonLdError, ParseException {

    // Set handle and create caching subfolder.
    String hdl = "hdl:21.T11991/0000-0011-73E8-6";
    String ttl = IOUtils.readStringFromStream(
        new FileInputStream(new File("src/test/resources/21.T11991-0000-0011-73E8-6.ttl")));
    String expectedJSON = IOUtils.readStringFromStream(new FileInputStream(
        new File("src/test/resources/21.T11991-0000-0011-73E8-6.manifest.json")));

    // Create JSON from TTL.
    String json = SCUtils.ttl2jsonld(ttl, URI.create(Utils.PRESENTATION_SERVICE_CONTEXT_URL),
        Utils.IMAGE_SERVICE_CONTEXT_URL, SCUtils.MANIFEST_FRAME_LOC);

    // Delete caching file and folder.
    Utils.deleteFileAndFolder(hdl);

    if (!json.trim().equals(expectedJSON.trim())) {
      System.out.println("EXPECTED JSON:\n" + expectedJSON);
      System.out.println("CREATED JSON:\n" + json);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws JsonLdError
   * @throws ParseException
   */
  @Test
  public void testTtl2jsonldTextgrid() throws IOException, JsonLdError, ParseException {

    // Set handle and create caching subfolder.
    String hdl = "textgrid:24gv9.0";
    String ttl = IOUtils.readStringFromStream(
        new FileInputStream(new File("src/test/resources/24gv9.0.ttl")));
    String expectedJSON = IOUtils.readStringFromStream(
        new FileInputStream(new File("src/test/resources/24gv9.0.manifest.json")));

    // Create JSON from TTL.
    String json = SCUtils.ttl2jsonld(ttl, URI.create(Utils.PRESENTATION_SERVICE_CONTEXT_URL),
        Utils.IMAGE_SERVICE_CONTEXT_URL, SCUtils.MANIFEST_FRAME_LOC);

    // Delete caching file and folder.
    Utils.deleteFileAndFolder(hdl);

    if (!json.trim().equals(expectedJSON.trim())) {
      System.out.println("EXPECTED JSON:\n" + expectedJSON);
      System.out.println("CREATED JSON:\n" + json);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Test
  public void testProcessDariahIndexTemplate()
      throws FileNotFoundException, IOException, ParserConfigurationException, SAXException {

    HashMap<String, Object> vars = new HashMap<String, Object>();
    String expectedDHREPIndexHtml = IOUtils
        .readStringFromStream(new FileInputStream(new File("src/test/resources/index-dhrep.html")));

    // Add general vars.
    vars.put("list_sets_url", "https://trep.de.dariah.eu/1.0/oaipmh/oai?verb=ListSets");
    vars.put("serviceBaseUrl", "https://trep.de.dariah.eu/1.0/iiif/manifests");
    vars.put("documentationUrl",
        "https://trep.de.dariah.eu/doc/services/submodules/dhiiifmd/docs_dhrep/index.html");
    vars.put("crud_url", "https://trep.de.dariah.eu/1.0/dhcrud/");
    vars.put("manifest_url", "https://trep.de.darah.eu/1.0/iiif/manifests/");
    vars.put("mirador_url", "https://dev.textgridlab.org/1.0/iiif/mirador");
    vars.put("tifyUrl", "https://demo.tify.rocks/demo.html");
    vars.put("presentationUrl", "https://trep.de.dariah.eu/1.0/dhcrud/");

    // Get test tree map from listSets XML file.
    String listSetsXML = IOUtils
        .readStringFromStream(new FileInputStream(new File("src/test/resources/listSets.xml")));
    Map<String, String> handlesAndTitles = SCUtils.getHandlesAndTitles(listSetsXML);

    // Add specific collection vars.
    vars.put("collections", SCUtils.makeMustacheMap4HTMLTemplate(handlesAndTitles));

    String html =
        SCUtils.processTemplate("src/main/resources/templates/index-dhrep.html.tmpl", vars);

    if (!expectedDHREPIndexHtml.trim().equals(html.trim())) {
      System.out.println("EXPECTED INDEX HTML:\n" + expectedDHREPIndexHtml);
      System.out.println("CREATED INDEX HTML:\n" + html);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Test
  public void testProcessDariahIndexTemplateBrokenURIs()
      throws FileNotFoundException, IOException, ParserConfigurationException, SAXException {

    HashMap<String, Object> vars = new HashMap<String, Object>();
    String expectedDHREPIndexHtmlFiltered = IOUtils.readStringFromStream(
        new FileInputStream(new File("src/test/resources/index-dhrep-brokenURIs.html")));

    // Add general vars.
    vars.put("list_sets_url", "https://trep.de.dariah.eu/1.0/oaipmh/oai?verb=ListSets");
    vars.put("serviceBaseUrl", "https://trep.de.dariah.eu/1.0/iiif/manifests");
    vars.put("documentationUrl",
        "https://trep.de.dariah.eu/doc/services/submodules/dhiiifmd/docs_dhrep/index.html");
    vars.put("crud_url", "https://trep.de.dariah.eu/1.0/dhcrud/");
    vars.put("manifest_url", "https://trep.de.darah.eu/1.0/iiif/manifests/");
    vars.put("mirador_url", "https://dev.textgridlab.org/1.0/iiif/mirador");
    vars.put("tifyUrl", "https://demo.tify.rocks/demo.html");
    vars.put("presentationUrl", "https://trep.de.dariah.eu/1.0/dhcrud/");

    // Get test tree map from listSets XML file.
    String listSetsXML = IOUtils
        .readStringFromStream(new FileInputStream(new File("src/test/resources/listSets.xml")));
    Map<String, String> allCollections = SCUtils.getHandlesAndTitles(listSetsXML);

    // Filter enabled and broken projects by URI.
    String[] listedProjects = null;
    HashSet<String> brokenURIs = new HashSet<String>();
    // Title: "Murks?"
    brokenURIs.add("hdl:21.T11991/0000-0007-970E-0");
    Map<String, String> collections =
        SCUtils.filterCollections(allCollections, listedProjects, brokenURIs);

    // Add specific collection vars.
    vars.put("collections", SCUtils.makeMustacheMap4HTMLTemplate(collections));

    String html =
        SCUtils.processTemplate("src/main/resources/templates/index-dhrep.html.tmpl", vars);

    if (!expectedDHREPIndexHtmlFiltered.trim().equals(html.trim())) {
      System.out.println("EXPECTED INDEX HTML:\n" + expectedDHREPIndexHtmlFiltered);
      System.out.println("CREATED INDEX HTML:\n" + html);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Test
  public void testProcessDariahIndexTemplateListedProjects()
      throws FileNotFoundException, IOException, ParserConfigurationException, SAXException {

    HashMap<String, Object> vars = new HashMap<String, Object>();
    String expectedDHREPIndexHtmlFiltered = IOUtils.readStringFromStream(
        new FileInputStream(new File("src/test/resources/index-dhrep-listedProjects.html")));

    // Add general vars.
    vars.put("list_sets_url", "https://trep.de.dariah.eu/1.0/oaipmh/oai?verb=ListSets");
    vars.put("serviceBaseUrl", "https://trep.de.dariah.eu/1.0/iiif/manifests");
    vars.put("documentationUrl",
        "https://trep.de.dariah.eu/doc/services/submodules/dhiiifmd/docs_dhrep/index.html");
    vars.put("crud_url", "https://trep.de.dariah.eu/1.0/dhcrud/");
    vars.put("manifest_url", "https://trep.de.darah.eu/1.0/iiif/manifests/");
    vars.put("mirador_url", "https://dev.textgridlab.org/1.0/iiif/mirador");
    vars.put("tifyUrl", "https://demo.tify.rocks/demo.html");
    vars.put("presentationUrl", "https://trep.de.dariah.eu/1.0/dhcrud/");

    // Get test tree map from listSets XML file.
    String listSetsXML = IOUtils
        .readStringFromStream(new FileInputStream(new File("src/test/resources/listSets.xml")));
    Map<String, String> allCollections = SCUtils.getHandlesAndTitles(listSetsXML);

    // Filter enabled and broken projects by URI.
    String[] listedProjects = {"hdl:21.T11991/0000-0006-0ACA-C", "hdl:21.T11991/0000-000C-A544-D",
        "hdl:21.T11991/0000-000D-25A9-B"};
    HashSet<String> brokenURIs = null;
    Map<String, String> collections =
        SCUtils.filterCollections(allCollections, listedProjects, brokenURIs);

    // Add specific collection vars.
    vars.put("collections", SCUtils.makeMustacheMap4HTMLTemplate(collections));

    String html =
        SCUtils.processTemplate("src/main/resources/templates/index-dhrep.html.tmpl", vars);

    if (!expectedDHREPIndexHtmlFiltered.trim().equals(html.trim())) {
      System.out.println("EXPECTED INDEX HTML:\n" + expectedDHREPIndexHtmlFiltered);
      System.out.println("CREATED INDEX HTML:\n" + html);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Test
  public void testGetHandlesAndTitles()
      throws FileNotFoundException, IOException, ParserConfigurationException, SAXException {

    String listSetsXML = IOUtils
        .readStringFromStream(new FileInputStream(new File("src/test/resources/listSets.xml")));

    Map<String, String> handlesAndTitles = SCUtils.getHandlesAndTitles(listSetsXML);

    // TODO Test more specific here :-)
    if (!handlesAndTitles.containsValue("Geodaten")) {
      assertTrue(false);
    }
  }

  /**
   * 
   */
  @Test
  public void testCleanIdentifier() {

    String identifierHDL = "hdl:21.T11991%2F0000-001C-2AB6-6";
    String identifierNOHDL = "21.T11991%2F0000-001C-2AB6-6";
    String expectedHDL = "hdl:21.T11991/0000-001C-2AB6-6";

    String testHDL = SCUtils.cleanIdentifier(identifierHDL);
    String testNOHDL = SCUtils.cleanIdentifier(identifierNOHDL);

    if (!testHDL.equals(testHDL)) {
      System.out.println(testHDL + "!=" + expectedHDL);
      assertTrue(false);
    }

    if (!testNOHDL.equals(testNOHDL)) {
      System.out.println(testNOHDL + "!=" + expectedHDL);
      assertTrue(false);
    }
  }

}
