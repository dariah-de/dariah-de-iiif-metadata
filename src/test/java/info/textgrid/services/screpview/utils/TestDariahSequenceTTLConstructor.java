package info.textgrid.services.screpview.utils;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import javax.xml.transform.TransformerConfigurationException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.elasticsearch.client.RestHighLevelClient;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.common.CachingUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/**
 * @author Stefan E. Funk, SUB Göttingen
 * 
 *         FIXME Get ES client in here somehow!!
 */
@Ignore
public class TestDariahSequenceTTLConstructor {

  RestHighLevelClient esClient;

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  /**
   * <p>
   * This is an online test, please ignore if not online :-)
   * </p>
   * 
   * @throws Exception
   * @throws TransformerConfigurationException
   */
  @Test
  public void testDariahSequenceTTLConstructorONLINE()
      throws TransformerConfigurationException, Exception {

    // Set handle and create caching subfolder.
    String hdl = "hdl:21.T11991/0000-0011-73E8-6";
    String expectedTTL = IOUtils.readStringFromStream(
        new FileInputStream(new File("src/test/resources/21.T11991-0000-0011-73E8-6.ttl")));
    String expectedJSON = IOUtils.readStringFromStream(new FileInputStream(
        new File("src/test/resources/21.T11991-0000-0011-73E8-6.manifest.json")));
    String logID = "IIIFMDTEST_" + System.currentTimeMillis();

    File cachingSubfolder = new File(Utils.CACHE_DIR, CachingUtils.getCachingPathFromURI(hdl));

    System.out.println("caching folder: " + cachingSubfolder.getCanonicalPath());

    cachingSubfolder.mkdirs();

    // Create STTL Constructor and set vars.
    DariahSequenceTTLConstructor sttlConstructor =
        new DariahSequenceTTLConstructor(this.esClient, "FINE");
    sttlConstructor.setServiceBaseUrl("https://trep.de.dariah.eu/1.0/iiif/manifests");
    sttlConstructor.setCrudUrl("https://trep.de.dariah.eu/1.0/dhcrud");
    sttlConstructor.setIiifServiceUrl("https://trep.de.dariah.eu/1.0/digilib/rest/IIIF");
    sttlConstructor.setRepositoryUrl("https://trep.de.dariah.eu");
    sttlConstructor.setPresentationUrl(sttlConstructor.getCrudUrl());
    sttlConstructor
        .setRepositoryLogo("https://res.de.dariah.eu/logos/dariah-de/dariah-de_cmyk_en.svg");
    sttlConstructor.setIiifConformanceUrl("http://iiif.io/api/image/2/level2.json");
    sttlConstructor.setCacheDir(Utils.CACHE_DIR);

    // Create manifest head.
    String manifestHead = sttlConstructor.getManifestHead(hdl, logID);

    // Create TTL.
    String manifestBody = sttlConstructor.ttl(hdl, logID);

    // Assemble TTL file for JSON creation.
    String ttl = manifestHead + "\n" + manifestBody;

    // Delete caching file and folder.
    Utils.deleteFileAndFolder(hdl);

    // Check for correctness of TTL.
    Model expectedModel = RDFUtils.readModel(expectedTTL, RDFConstants.TURTLE);
    Model model = RDFUtils.readModel(ttl, RDFConstants.TURTLE);
    if (!model.isIsomorphicWith(expectedModel)) {
      System.out.println("EXPECTED TTL:\n" + expectedTTL);
      System.out.println("CREATED TTL:\n" + ttl);
      assertTrue(false);
    }

    // Create JSON from TTL.
    String json = SCUtils.ttl2jsonld(ttl, URI.create(Utils.PRESENTATION_SERVICE_CONTEXT_URL),
        Utils.IMAGE_SERVICE_CONTEXT_URL, SCUtils.MANIFEST_FRAME_LOC);

    // Check for correctness of JSON.
    if (!json.trim().equals(expectedJSON.trim())) {
      System.out.println("EXPECTED JSON:\n" + expectedJSON);
      System.out.println("CREATED JSON:\n" + json);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   * 
   */
  @Test
  public void testParseInfoJSON() throws FileNotFoundException, IOException, ParseException {

    String theJSON =
        IOUtils.readStringFromStream(new FileInputStream(new File("src/test/resources/info.json")));
    String expectedWidth = "7296";
    String expectedHeight = "5688";

    // Test width key.
    String key = DariahSequenceTTLConstructor.WIDTH;
    String width = DariahSequenceTTLConstructor.parseInfoJSON(theJSON, key,
        "URGL_" + System.currentTimeMillis());

    if (!expectedWidth.equals(width)) {
      System.out.println(expectedWidth + " != " + width);
      assertTrue(false);
    }

    // Test height key.
    key = DariahSequenceTTLConstructor.HEIGHT;
    String height = DariahSequenceTTLConstructor.parseInfoJSON(theJSON, key,
        "URGL_" + System.currentTimeMillis());
    if (!expectedHeight.equals(height)) {
      System.out.println(expectedHeight + " != " + height);
      assertTrue(false);
    }
  }

}
