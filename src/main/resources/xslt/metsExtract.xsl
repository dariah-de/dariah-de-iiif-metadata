<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tg="http://textgrid.info/relation-ns#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:mets="http://www.loc.gov/METS/"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:mods="http://www.loc.gov/mods/v3"
    xmlns:dv="http://dfg-viewer.de/" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">

    <xsl:output method="text" omit-xml-declaration="yes" indent="no"/>

    <xsl:template match="/">
        <!--xsl:apply-templates/-->
        <xsl:choose>
            <xsl:when test="//mets:mets">
                <xsl:for-each
                    select="//mets:structMap[@TYPE = 'PHYSICAL']/mets:div/mets:div//mets:fptr[ends-with(@FILEID, 'MAX')]">
                    <xsl:variable name="fid" select="@FILEID"/>
                    <xsl:value-of
                        select="root()//mets:fileSec/mets:fileGrp[@USE = 'MAX']/mets:file[@ID = $fid]/mets:FLocat/@xlink:href"/>
                    <xsl:text>&#xA;</xsl:text>
                </xsl:for-each>
            </xsl:when>
            <xsl:when test="//tei:TEI">
                <xsl:choose>
                    <xsl:when test="//tei:TEI/tei:sourceDoc">
                        <!-- if encoded according to chapter 11 of the guidelines -->
                        <xsl:for-each select="//tei:surface/tei:graphic/@url">
                            <!-- new encoding mechanism established by Martin de la Iglesia -->
                            <xsl:value-of select="."/>
                            <xsl:text>&#xA;</xsl:text>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:for-each select="//tei:pb">
                            <xsl:choose>
                                <xsl:when test="starts-with(./@corresp, 'textgrid:')">
                                    <xsl:value-of select="./@corresp"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="./@facs"/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:text>&#xA;</xsl:text>
                        </xsl:for-each>
                    </xsl:otherwise>
                </xsl:choose>

            </xsl:when>
            <xsl:otherwise> unsupported </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    
</xsl:stylesheet>
