package info.textgrid.services.screpview;

/*
 * #%L SharedCanvas Repository View
 *
 * %% Copyright (C) 2013 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import org.apache.cxf.helpers.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import com.github.jsonldjava.core.JsonLdError;
import de.digitalcollections.iiif.model.sharedcanvas.Collection;
import de.digitalcollections.iiif.model.sharedcanvas.Manifest;
import info.textgrid.middleware.common.CachingUtils;
import info.textgrid.services.screpview.utils.DariahSequenceTTLConstructor;
import info.textgrid.services.screpview.utils.SCUtils;
import info.textgrid.services.screpview.utils.SequenceTTLConstructor;
import info.textgrid.services.screpview.utils.SequenceTTLConstructorAbs;
import info.textgrid.services.screpview.utils.TextgridSequenceTTLConstructor;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 *
 */
public class Service {

  // **
  // FINALS
  // **

  private static final String TTL_PREFIX = "@prefix dc: <http://purl.org/dc/elements/1.1/>.\n"
      + "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.\n"
      + "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.\n"
      + "@prefix svcs: <http://rdfs.org/sioc/services#>.\n"
      + "@prefix sc: <http://iiif.io/api/presentation/2#>.\n\n";
  public static final String WRONG_ID_PREFIX =
      "ID has to be TextGrid URI [" + CachingUtils.TG_PATH_PREFIX + "] or DARIAH-DE Handle ["
          + CachingUtils.DH_PATH_PREFIX + "]!";
  public static final String charset = "UTF-8";
  private static final String SCOPE_TEXTGRID = "textgrid";
  private static final String SCOPE_DARIAH = "dariah";
  protected static final String IIIFMD = "IIIFMD";
  // Use for DEBUGGING only!
  private static final boolean ENABLE_CACHING = true;

  // **
  // STATICS
  // **

  protected static Logger log = Logger.getLogger(Service.class.getName());

  // **
  // CLASS
  // **

  private TextgridSequenceTTLConstructor tgSTTLConstructor;
  private DariahSequenceTTLConstructor dhSTTLConstructor;
  private static String jsonldContextUrl;
  private String iiifContextUrl;
  private String[] listedProjects;
  private HashSet<String> brokenUris;
  private String miradorUrl;
  private String tifyUrl;
  private String publishedLabel;
  private String publishedDescription;
  private String sandboxLabel;
  private String sandboxDescription;
  private String scope;

  /**
   * <p>
   * The Constructor.
   * </p>
   *
   * @param tgSequenceTTLConstructor
   * @param dhSequenceTTLConstructor
   * @param logLevel
   * @throws Exception
   */
  public Service(TextgridSequenceTTLConstructor tgSequenceTTLConstructor,
      DariahSequenceTTLConstructor dhSequenceTTLConstructor, String inputLoglevel)
      throws Exception {
    this.tgSTTLConstructor = tgSequenceTTLConstructor;
    this.dhSTTLConstructor = dhSequenceTTLConstructor;

    log.info("starting IIIF Metadata Service");

    log.setLevel(Level.parse(inputLoglevel));
    log.info("setting loglevel to " + inputLoglevel);
  }

  /**
   * <p>
   * The Manifest for an ID.
   * </p>
   *
   * @see http://www.shared-canvas.org/datamodel/iiif/metadata-api.html#Manifest
   *
   * @param id
   * @return
   * @throws IOException
   * @throws TransformerException
   * @throws ParseException
   * @throws URISyntaxException
   * @throws JsonLdError
   * @throws XMLStreamException
   */
  @GET
  @Path("/{id}/manifest.json")
  @Produces({"application/json; charset=UTF-8", "application/ld+json; charset=UTF-8"})
  public Response manifest(@PathParam("id") final String id) {

    Response result = null;

    SequenceTTLConstructor sttlConstructor;
    String logID = SCUtils.createRandomLogID();
    String hdlWithPrefix = id;
    String json;

    try {
      // Decide on scope on used constructor.
      if (this.scope.equals(SCOPE_TEXTGRID)) {
        sttlConstructor = this.tgSTTLConstructor;
      } else if (this.scope.equals(SCOPE_DARIAH)) {
        sttlConstructor = this.dhSTTLConstructor;
        hdlWithPrefix = SCUtils.cleanIdentifier(hdlWithPrefix);
      } else {
        result = Response.status(Status.BAD_REQUEST).build();
        return result;
      }

      log.info("[" + logID + "] requesting manifest for uri " + hdlWithPrefix);

      // Get JSON file path.
      File jsonFile = new File(SCUtils.getJSONManifestPath(sttlConstructor, hdlWithPrefix));

      log.fine("[" + logID + "] looking for json manifest " + jsonFile.getCanonicalPath());

      if (ENABLE_CACHING && jsonFile.exists()) {
        log.info("[" + logID + "] using cached file " + jsonFile.getAbsolutePath());
        json = IOUtils.toString(new FileInputStream(jsonFile), charset);
      } else {

        log.fine("[" + logID + "] no cachefile found, creating manifest");

        // Get RDF metadata for TG/DH object.
        String dataset = sttlConstructor.getManifestHead(hdlWithPrefix, logID);

        log.fine("[" + logID + "] manifest head creation complete");

        // Create cache dir if not yet existing.
        File cacheDir = SCUtils.createCacheDir(jsonFile);
        log.fine("[" + logID + "] cache directory creation complete" + cacheDir.getCanonicalPath());

        // Get sequence from TG/DH object.
        String sequencettl = sttlConstructor.ttl(hdlWithPrefix, logID);
        dataset += sequencettl;

        log.fine("[" + logID + "] ttl creation complete");

        // Write complete TTL dataset to disk, if configured so.
        if (SequenceTTLConstructorAbs.STORE_TTL_TO_DISK) {
          File ttlFile = new File(new File(sttlConstructor.getCacheDir()).getAbsolutePath(),
              CachingUtils.getCachingPathFromURI(id) + SequenceTTLConstructorAbs.TTL_FILESUFFIX);
          Files.write(ttlFile.toPath(), dataset.getBytes());
        }

        json = SCUtils.ttl2jsonld(dataset, new URI(jsonldContextUrl), this.iiifContextUrl,
            SCUtils.MANIFEST_FRAME_LOC);

        log.info("[" + logID + "] json creation complete");

        Files.write(jsonFile.toPath(), json.getBytes(charset));

        log.fine(
            "[" + logID + "] json manifest file written to cache: " + jsonFile.getAbsolutePath());
      }

      result = Response.ok(json).build();

    } catch (IOException | JsonLdError | URISyntaxException | TransformerException
        | ParseException | XMLStreamException e) {
      String message = "[" + logID + "] error creating ttl manifest file! " + e.getClass().getName()
          + ": " + e.getMessage();
      log.severe(message);
    }

    return result;
  }

  /**
   * <p>
   * The Collection for an ID.
   * </p>
   *
   * @see https://iiif.io/api/presentation/2.1/#collection
   *
   * @param id
   * @return
   * @throws IOException
   * @throws TransformerException
   * @throws ParseException
   * @throws URISyntaxException
   * @throws JsonLdError
   * @throws XMLStreamException
   */
  @GET
  @Path("/{id}/collection.json")
  @Produces({"application/json; charset=UTF-8", "application/ld+json; charset=UTF-8"})
  public Response collection(@PathParam("id") final String id) {

    Response result = null;

    SequenceTTLConstructor sttlConstructor;
    String logID = SCUtils.createRandomLogID();
    String json;
    String hdlWithPrefix = id;

    try {
      // Decide on scope on used constructor.
      if (this.scope.equals(SCOPE_TEXTGRID)) {
        sttlConstructor = this.tgSTTLConstructor;
      } else if (this.scope.equals(SCOPE_DARIAH)) {
        sttlConstructor = this.dhSTTLConstructor;
        hdlWithPrefix = SCUtils.cleanIdentifier(hdlWithPrefix);
      } else {
        result = Response.status(Status.BAD_REQUEST).build();
        return result;
      }

      log.info("[" + logID + "] requesting collection for uri " + hdlWithPrefix);

      // Get JSON file path.
      File jsonFile = new File(SCUtils.getJSONCollectionPath(sttlConstructor, hdlWithPrefix));

      log.fine("[" + logID + "] looking for collection " + jsonFile.getCanonicalPath());

      if (ENABLE_CACHING && jsonFile.exists()) {
        log.fine("[" + logID + "] using cached file " + jsonFile.getAbsolutePath());
        json = IOUtils.toString(new FileInputStream(jsonFile), charset);
      } else {

        log.fine("[" + logID + "] no cachefile found, creating collection");

        // Create cache dir if not yet existing.
        File cacheDir = SCUtils.createCacheDir(jsonFile);
        log.fine("[" + logID + "] cache directory creation complete" + cacheDir.getCanonicalPath());

        // Get basic metadata for collection.
        Collection collection = sttlConstructor.getCollection(hdlWithPrefix, logID);

        log.fine("[" + logID + "] " + hdlWithPrefix + " collection skeleton creation complete");

        json = SCUtils.getMappedString(collection);

        log.info("[" + logID + "] json creation complete");

        Files.write(jsonFile.toPath(), json.getBytes(charset));

        log.fine(
            "[" + logID + "] json collection file written to cache: " + jsonFile.getAbsolutePath());
      }

      result = Response.ok(json).build();

    } catch (IOException e) {
      String message = "[" + logID + "] error creating collection file! " + e.getClass().getName()
          + ": " + e.getMessage();
      log.severe(message);
    }

    return result;
  }

  /**
   * @return
   */
  @GET
  @Path("/")
  @Produces("text/html; charset=UTF-8")
  public Response htmlIndex() {

    Response result = null;

    HashMap<String, Object> vars = new HashMap<String, Object>();
    String logID = SCUtils.createRandomLogID();

    log.info("[" + logID + "] requesting root html collection page");

    // Get the TGREP index manifest page.
    if (this.scope.equals(SCOPE_TEXTGRID)) {

      Map<String, String> publicItems =
          this.tgSTTLConstructor.getCollections(this.listedProjects, false, logID);
      publicItems.keySet().removeAll(this.brokenUris);
      vars.put("publicItems", publicItems.entrySet());
      Map<String, String> sandboxItems =
          this.tgSTTLConstructor.getCollections(this.listedProjects, true, logID);
      sandboxItems.keySet().removeAll(this.brokenUris);
      vars.put("sandboxItems", sandboxItems.entrySet());

      // Assemble HTML template configuration.
      vars.put("serviceBaseUrl", this.tgSTTLConstructor.getServiceBaseUrl());
      vars.put("miradorUrl", getMiradorUrl());
      vars.put("tifyUrl", getTifyUrl());
      vars.put("repositoryUrl", this.tgSTTLConstructor.getRepositoryUrl());
      vars.put("presentationUrl", this.tgSTTLConstructor.getPresentationUrl());
      vars.put("documentationUrl", this.tgSTTLConstructor.getdocumentationUrl());

      String html = SCUtils.processTemplate("/templates/index-tgrep.html.tmpl", vars);

      result = Response.ok(html).build();

      log.info("[" + logID + "] tgrep root html delivererd");
    }

    // Get the DHREP index manifest page.
    else if (this.scope.equals(SCOPE_DARIAH)) {

      Map<String, String> collections = new HashMap<String, String>();

      // Get all root collections from TTL constructor first, we need the title anyway.
      Map<String, String> allCollections =
          this.dhSTTLConstructor.getCollections(null, false, logID);

      // Assemble HTML template configuration.
      vars.put("serviceBaseUrl", this.dhSTTLConstructor.getServiceBaseUrl());
      vars.put("list_sets_url", this.dhSTTLConstructor.getOaipmhUrl() + "?verb=ListSets");
      vars.put("crud_url", this.dhSTTLConstructor.getCrudUrl());
      vars.put("manifest_url", this.dhSTTLConstructor.getServiceBaseUrl());
      vars.put("mirador_url", getMiradorUrl() + "?uri=");
      vars.put("presentationUrl", this.dhSTTLConstructor.getPresentationUrl());
      vars.put("documentationUrl", this.dhSTTLConstructor.getdocumentationUrl());

      // Filter enabled and broken projects by URI.
      collections = SCUtils.filterCollections(allCollections, this.listedProjects, this.brokenUris);

      vars.put("collections", SCUtils.makeMustacheMap4HTMLTemplate(collections));
      String html = SCUtils.processTemplate("/templates/index-dhrep.html.tmpl", vars);

      result = Response.ok(html).build();

      log.info("[" + logID + "] dhrep root html delivererd");

    } else {
      result = Response.status(Status.BAD_REQUEST).build();
    }

    return result;
  }

  /**
   * <p>
   * Deliver the repository's ROOT collection(s).
   * </p>
   * 
   * @return
   * @throws IOException
   * @throws ParseException
   */
  @GET
  @Path("/collection.json")
  @Produces("application/json; charset=UTF-8")
  public Response listCollections() throws IOException, ParseException {

    Response result = null;

    String logID = SCUtils.createRandomLogID();

    log.info("[" + logID + "] requesting repository root collection");

    // Decide if returning a TG or DH JSON page.
    if (this.scope.equals(SCOPE_TEXTGRID)) {

      Collection topcol =
          new Collection(this.tgSTTLConstructor.getServiceBaseUrl() + "/collection.json");
      topcol.addLabel("TextGrid objects with IIIF metadata");

      // published collections
      Collection pubcol =
          new Collection(this.tgSTTLConstructor.getServiceBaseUrl() + "/collection/published.json");
      pubcol.addLabel(this.publishedLabel);
      pubcol.addDescription(this.publishedDescription);

      // sandbox Collections
      Collection sandcol =
          new Collection(this.tgSTTLConstructor.getServiceBaseUrl() + "/collection/sandbox.json");
      sandcol.addLabel(this.sandboxLabel);
      sandcol.addDescription(this.sandboxDescription);

      topcol.addCollection(pubcol);
      topcol.addCollection(sandcol);

      String response = SCUtils.getMappedString(topcol);

      result = Response.ok(response).build();

      log.info("[" + logID + "] tgrep repository root collection delivered");

    } else if (this.scope.equals(SCOPE_DARIAH)) {

      Collection topcol =
          new Collection(this.dhSTTLConstructor.getServiceBaseUrl() + "/collection.json");
      topcol.addLabel("DARIAH-DE Repository IIIF Collections");
      topcol.addDescription("DARIAH-DE Repository IIIF Collections");
      topcol._context = jsonldContextUrl;

      // Get all root collections from TTL constructor.
      Map<String, String> collections =
          this.dhSTTLConstructor.getCollections(this.listedProjects, false, logID);
      for (String hdl : collections.keySet()) {
        // Create new (root) collection for every title/hdl set.
        String title = collections.get(hdl);
        Collection col = new Collection(this.dhSTTLConstructor.getServiceBaseUrl() + "/"
            + hdl.replace("/", "%2F") + "/collection.json");
        col.addLabel(title);
        col.addDescription(title + " (" + hdl + ")");
        // Add root collection to collections.
        topcol.addCollection(col);
      }

      String response = SCUtils.getMappedString(topcol);

      result = Response.ok(response).build();

      log.info("[" + logID + "] dhrep repository root collection delivered");

    } else {
      result = Response.status(Status.BAD_REQUEST).build();
    }

    return result;
  }

  /**
   * <p>
   * Assembles all published TG-rep collections.
   * </p>
   * 
   * @return
   * @throws IOException
   * @throws ParseException
   */
  @GET
  @Path("/collection/published.json")
  @Produces("application/json; charset=UTF-8")
  public Response listPublishedCollections() throws IOException, ParseException {

    Response result = null;

    String logID = SCUtils.createRandomLogID();

    log.info("[" + logID + "] requesting published collections");

    // Decide if returning a TG or DH JSON page.
    if (this.scope.equals(SCOPE_TEXTGRID)) {

      Collection col =
          new Collection(this.tgSTTLConstructor.getServiceBaseUrl() + "/collection/published.json");
      col.addLabel(this.publishedLabel);
      col.addDescription(this.publishedDescription);
      for (Entry<String, String> entry : this.tgSTTLConstructor
          .getCollections(this.listedProjects, false, logID).entrySet()) {
        if (!this.brokenUris.contains(entry.getKey())) {
          String manifestUrl =
              this.tgSTTLConstructor.getServiceBaseUrl() + "/" + entry.getKey() + "/manifest.json";
          col.addManifest(new Manifest(manifestUrl, entry.getValue()));
        }
      }

      String response = SCUtils.getMappedString(col);

      result = Response.ok(response).build();

      log.info("[" + logID + "] tgrep published collections delivered");

    } else if (this.scope.equals(SCOPE_DARIAH)) {
      result = Response.status(Status.NOT_IMPLEMENTED).build();
    } else {
      result = Response.status(Status.BAD_REQUEST).build();
    }

    return result;
  }

  /**
   * <p>
   * Assembles all TG-rep collections from the sandbox.
   * </p>
   * 
   * @return
   * @throws IOException
   * @throws ParseException
   */
  @GET
  @Path("/collection/sandbox.json")
  @Produces("application/json; charset=UTF-8")
  public Response listSandboxCollections() throws IOException, ParseException {

    Response result = null;

    String logID = SCUtils.createRandomLogID();

    log.info("[" + logID + "] requesting sandbox collections");

    // Decide if returning a TG or DH JSON page.
    if (this.scope.equals(SCOPE_TEXTGRID)) {

      Collection col =
          new Collection(this.tgSTTLConstructor.getServiceBaseUrl() + "/collection/sandbox.json");
      col.addLabel(this.sandboxLabel);
      col.addDescription(this.sandboxDescription);
      for (Entry<String, String> entry : this.tgSTTLConstructor
          .getCollections(this.listedProjects, true, logID).entrySet()) {
        if (!this.brokenUris.contains(entry.getKey())) {
          String manifestUrl =
              this.tgSTTLConstructor.getServiceBaseUrl() + "/" + entry.getKey() + "/manifest.json";
          col.addManifest(new Manifest(manifestUrl, entry.getValue()));
        }
      }

      String response = SCUtils.getMappedString(col);

      result = Response.ok(response).build();

      log.info("[" + logID + "] tgrep sandbox collections delivered");

    } else if (this.scope.equals(SCOPE_DARIAH)) {
      result = Response.status(Status.NOT_IMPLEMENTED).build();
    } else {
      result = Response.status(Status.BAD_REQUEST).build();
    }

    return result;
  }

  /**
   * <p>
   * Assembles the sequence.
   * </p>
   *
   * @see http://www.shared-canvas.org/datamodel/iiif/metadata-api.html#Sequence
   *
   * @param id
   * @param name
   * @return
   * @throws MalformedURLException
   * @throws IOException
   * @throws TransformerException
   * @throws ParseException
   * @throws JSONLDProcessingError
   * @throws URISyntaxException
   * @throws JsonLdError
   * @throws XMLStreamException
   */
  @GET
  @Path("/{id}/sequence/{name}.json")
  @Produces({"application/json; charset=UTF-8", "application/ld+json; charset=UTF-8"})
  public Response sequence(@PathParam("id") final String id, @PathParam("name") final String name)
      throws MalformedURLException, IOException, TransformerException, ParseException,
      URISyntaxException, JsonLdError, XMLStreamException {

    Response result = null;

    SequenceTTLConstructor sttlConstructor;
    String logID = SCUtils.createRandomLogID();
    String json;
    String hdlWithPrefix = id;

    // Decide on scope on used constructor.
    if (this.scope.equals(SCOPE_TEXTGRID)) {
      sttlConstructor = this.tgSTTLConstructor;
    } else if (this.scope.equals(SCOPE_DARIAH)) {
      sttlConstructor = this.dhSTTLConstructor;
      hdlWithPrefix = SCUtils.cleanIdentifier(hdlWithPrefix);
    } else {
      result = Response.status(Status.BAD_REQUEST).build();
      return result;
    }

    log.info("[" + logID + "] requesting sequence for " + hdlWithPrefix);

    if (!name.equals("normal")) {
      return Response.status(Status.NOT_FOUND).entity("only normal sequence available").build();
    }

    // Get.
    File f = new File(sttlConstructor.getCacheDir(), hdlWithPrefix.substring(9) + ".sequence.json");
    if (f.exists()) {
      log.info("[" + logID + "] using cached file " + f.getAbsolutePath());
      json = IOUtils.toString(new FileInputStream(f), charset);
    } else {
      log.info("[" + logID + "] no cachefile found! creating " + f.getAbsolutePath());

      String sequencettl = TTL_PREFIX + sttlConstructor.ttl(hdlWithPrefix, logID);
      json = SCUtils.ttl2jsonld(sequencettl, new URI(jsonldContextUrl), this.iiifContextUrl,
          SCUtils.SEQUENCE_FRAME_LOC);
      Files.write(f.toPath(), json.getBytes(charset));
    }

    result = Response.ok(json).build();

    return result;
  }

  /**
   * <p>
   * Assembles the canvas.
   * </p>
   *
   * @see http://www.shared-canvas.org/datamodel/iiif/metadata-api.html#Canvas
   *
   * @param id
   * @param name
   * @return
   * @throws MalformedURLException
   * @throws IOException
   * @throws TransformerException
   * @throws ParseException
   * @throws JSONLDProcessingError
   * @throws URISyntaxException
   * @throws JsonLdError
   */
  @GET
  @Path("/{id}/canvas/{name}.json")
  @Produces({"application/json; charset=UTF-8", "application/ld+json; charset=UTF-8"})
  public Response canvas(@PathParam("id") final String id, @PathParam("name") final String name)
      throws MalformedURLException, IOException, TransformerException, ParseException,
      URISyntaxException, JsonLdError {

    Response result = null;

    SequenceTTLConstructor sttlConstructor;
    String logID = SCUtils.createRandomLogID();

    // Decide on scope on used constructor.
    if (this.scope.equals(SCOPE_TEXTGRID)) {
      sttlConstructor = this.tgSTTLConstructor;
    } else if (this.scope.equals(SCOPE_DARIAH)) {
      sttlConstructor = this.dhSTTLConstructor;
    } else {
      result = Response.status(Status.BAD_REQUEST).build();
      return result;
    }

    String canvasttl = sttlConstructor.getCanvas(id, name, logID);
    String json = SCUtils.ttl2jsonld(canvasttl, new URI(jsonldContextUrl), this.iiifContextUrl,
        SCUtils.CANVAS_FRAME_LOC);

    result = Response.ok(json).build();

    return result;
  }

  /**
   * <p>
   * Assembles the annotation.
   * </p>
   * 
   * @param id
   * @param name
   * @return
   * @throws IOException
   * @throws URISyntaxException
   * @throws JsonLdError
   */
  @GET
  @Path("/{id}/annotation/{name}.json")
  @Produces({"application/json; charset=UTF-8", "application/ld+json; charset=UTF-8"})
  public Response annotation(@PathParam("id") final String id, @PathParam("name") final String name)
      throws IOException, URISyntaxException, JsonLdError {

    Response result = null;

    SequenceTTLConstructor sttlConstructor;
    String logID = SCUtils.createRandomLogID();

    // Decide on scope on used constructor.
    if (this.scope.equals(SCOPE_TEXTGRID)) {
      sttlConstructor = this.tgSTTLConstructor;
    } else if (this.scope.equals(SCOPE_DARIAH)) {
      sttlConstructor = this.dhSTTLConstructor;
    } else {
      result = Response.status(Status.BAD_REQUEST).build();
      return result;
    }

    String canvasttl = sttlConstructor.getAnnotation(id, name, logID);
    String json = SCUtils.ttl2jsonld(canvasttl, new URI(jsonldContextUrl), this.iiifContextUrl,
        SCUtils.ANNOTATION_FRAME_LOC);

    result = Response.ok(json).build();

    return result;
  }

  /**
   * <p>
   * Lists all projects.
   * </p>
   * 
   * @return
   */
  @GET
  @Path("/projects/")
  @Produces("application/json; charset=UTF-8")
  public Response listProjects() {

    Response result = null;

    // Decide if returning a TG or DH JSON page.
    if (this.scope.equals(SCOPE_TEXTGRID)) {

      JSONObject response = new JSONObject();
      JSONArray projects = new JSONArray();
      projects.addAll(Arrays.asList(this.listedProjects));
      response.put("projects", projects);

      result = Response.ok(response.toJSONString()).build();

    } else if (this.scope.equals(SCOPE_DARIAH)) {
      result = Response.status(Status.NOT_IMPLEMENTED).build();
    } else {
      result = Response.status(Status.BAD_REQUEST).build();
    }

    return result;
  }

  // ***
  // GETTERS and SETTERS
  // ***

  /**
   * @return
   */
  public static String getJsonldContextUrl() {
    return jsonldContextUrl;
  }

  /**
   * @param contextUrl
   */
  public static void setJsonldContextUrl(String contextUrl) {
    jsonldContextUrl = contextUrl;
  }

  /**
   * @return
   */
  public String getIiifContextUrl() {
    return this.iiifContextUrl;
  }

  /**
   * @param iiifContextUrl
   */
  public void setIiifContextUrl(String iiifContextUrl) {
    this.iiifContextUrl = iiifContextUrl;
  }

  /**
   * @return
   */
  public String[] getListedProjects() {
    return this.listedProjects;
  }

  /**
   * @param listedProjects
   */
  public void setListedProjects(String[] listedProjects) {
    this.listedProjects = listedProjects;
  }

  /**
   * @return
   */
  public String getMiradorUrl() {
    return this.miradorUrl;
  }

  /**
   * @param miradorUrl
   */
  public void setMiradorUrl(String miradorUrl) {
    this.miradorUrl = miradorUrl;
  }

  /**
   * @return
   */
  public String getTifyUrl() {
    return this.tifyUrl;
  }

  /**
   * @param tifyUrl
   */
  public void setTifyUrl(String tifyUrl) {
    this.tifyUrl = tifyUrl;
  }

  /**
   * @param brokenUris
   */
  public void setBrokenUris(String[] brokenUris) {
    this.brokenUris = new HashSet<String>(Arrays.asList(brokenUris));
  }

  /**
   * @return
   */
  public String getPublishedLabel() {
    return this.publishedLabel;
  }

  /**
   * @param publishedLabel
   */
  public void setPublishedLabel(String publishedLabel) {
    this.publishedLabel = publishedLabel;
  }

  /**
   * @return
   */
  public String getPublishedDescription() {
    return this.publishedDescription;
  }

  /**
   * @param publishedDescription
   */
  public void setPublishedDescription(String publishedDescription) {
    this.publishedDescription = publishedDescription;
  }

  /**
   * @return
   */
  public String getSandboxLabel() {
    return this.sandboxLabel;
  }

  /**
   * @param sandboxLabel
   */
  public void setSandboxLabel(String sandboxLabel) {
    this.sandboxLabel = sandboxLabel;
  }

  /**
   * @return
   */
  public String getSandboxDescription() {
    return this.sandboxDescription;
  }

  /**
   * @param sandboxDescription
   */
  public void setSandboxDescription(String sandboxDescription) {
    this.sandboxDescription = sandboxDescription;
  }

  /**
   * @return
   */
  public String getScope() {
    return this.scope;
  }

  /**
   * <p>
   * Checks the given scope for validity.
   * </p>
   * 
   * @param scope
   * @throws Exception
   */
  public void setScope(String scope) throws Exception {

    // Scope must be "textgrid" or "dariah"!
    if (!scope.trim().equals(SCOPE_DARIAH) && !scope.trim().equals(SCOPE_TEXTGRID)) {
      throw new Exception("Service scope is configured incorrectly [" + scope.trim()
          + "]: Must be '" + SCOPE_DARIAH + "' or '" + SCOPE_TEXTGRID + "'");
    }

    this.scope = scope.trim();
  }

}
