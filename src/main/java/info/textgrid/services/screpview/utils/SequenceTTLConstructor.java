package info.textgrid.services.screpview.utils;

/*
 * #%L SharedCanvas Repository View
 * 
 * %% Copyright (C) 2013 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import org.json.simple.parser.ParseException;
import de.digitalcollections.iiif.model.sharedcanvas.Collection;

/**
 * 
 */
public interface SequenceTTLConstructor {

  /**
   * @param identifier
   * @param logID
   * @return
   * @throws IOException
   */
  String getManifestHead(String identifier, String logID) throws IOException;

  /**
   * @param identifier
   * @param name
   * @param logID
   * @return
   * @throws IOException
   */
  String getCanvas(String identifier, String name, String logID) throws IOException;

  /**
   * @param identifier
   * @param name
   * @param logID
   * @return
   * @throws IOException
   */
  String getAnnotation(String identifier, String name, String logID) throws IOException;

  /**
   * @param identifier
   * @param uris
   * @param logID
   * @return
   */
  String getTitles(String identifier, List<String> uris, String logID);

  /**
   * @param listedProjects
   * @param sandbox
   * @param logID
   * @return
   */
  Map<String, String> getCollections(String[] listedProjects, boolean sandbox, String logID);

  /**
   * @param identifier
   * @param logID
   * @return
   * @throws IOException
   */
  Collection getCollection(String identifier, String logID) throws IOException;

  /**
   * @return
   */
  String getServiceBaseUrl();

  /**
   * @param serviceBaseUrl
   */
  void setServiceBaseUrl(String serviceBaseUrl);

  /**
   * @return
   */
  String getCrudUrl();

  /**
   * @param crudUrl
   */
  void setCrudUrl(String crudUrl);

  /**
   * @return
   */
  String getIiifServiceUrl();

  /**
   * @param iiifServiceUrl
   */
  void setIiifServiceUrl(String iiifServiceUrl);

  /**
   * @return
   */
  String getIiifServiceUrlIntern();

  /**
   * @param iiifServiceUrlIntern
   */
  void setIiifServiceUrlIntern(String iiifServiceUrlIntern);

  /**
   * @return
   */
  String getIiifConformanceUrl();

  /**
   * @param iiifConformanceUrl
   */
  void setIiifConformanceUrl(String iiifConformanceUrl);

  /**
   * @return
   */
  String getRepositoryUrl();

  /**
   * @param repositoryUrl
   */
  void setRepositoryUrl(String repositoryUrl);

  /**
   * @param id
   * @param logID
   * @return
   * @throws MalformedURLException
   * @throws IOException
   * @throws TransformerException
   * @throws ParseException
   * @throws XMLStreamException
   */
  String ttl(String id, String logID) throws MalformedURLException, IOException,
      TransformerException, ParseException, XMLStreamException;

  /**
   * @param id
   * @param logID
   * @return
   * @throws MalformedURLException
   * @throws IOException
   * @throws TransformerException
   * @throws ParseException
   */
  List<String> getUriList(String id, String logID)
      throws MalformedURLException, IOException, TransformerException, ParseException;

  /**
   * @return
   */
  String getCacheDir();

}
