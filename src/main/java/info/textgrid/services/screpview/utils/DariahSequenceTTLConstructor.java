package info.textgrid.services.screpview.utils;

/*
 * #%L SharedCanvas Repository View
 * 
 * %% Copyright (C) 2018 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.jena.rdf.model.Model;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.util.StringUtils;
import org.xml.sax.SAXException;
import de.digitalcollections.iiif.model.sharedcanvas.Collection;
import de.digitalcollections.iiif.model.sharedcanvas.Manifest;
import info.textgrid.middleware.common.CachingUtils;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.DHCrudService;
import info.textgrid.services.screpview.Service;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * <p>
 * This class creates a TTL description from DARIAH-DE Repository collection and first level image
 * files.
 * </p>
 */
public class DariahSequenceTTLConstructor extends SequenceTTLConstructorAbs {

  // **
  // STATICS
  // **

  protected static Logger log = Logger.getLogger(Service.class.getName());
  private static DHCrudService dhcrudClient;
  private static final String DEFAULT_IMAGE_X_SIZE = "400";
  private static final String DEFAULT_IMAGE_Y_SIZE = "644";

  public static final String WIDTH = "width";
  public static final String HEIGHT = "height";
  public static final String INFO_JSON = "info.json";

  private static final String DMD = "descriptiveMetadata";
  private static final String DC_TITLE = RDFConstants.DC_PREFIX + ":" + RDFConstants.ELEM_DC_TITLE;
  private static final String DC_DESCRIPTION =
      RDFConstants.DC_PREFIX + ":" + RDFConstants.ELEM_DC_DESCRIPTION;
  private static final String DC_RIGHTS =
      RDFConstants.DC_PREFIX + ":" + RDFConstants.ELEM_DC_RIGHTS;
  private static final String ADMMD = "administrativeMetadata";
  private static final String DCTERMS_FORMAT =
      RDFConstants.DCTERMS_PREFIX + ":" + RDFConstants.ELEM_DCTERMS_FORMAT;

  // Use for DEBUGGING only!
  private static final boolean ENABLE_CACHING = true;

  /**
   * @param esClient
   * @param inputLoglevel
   * @throws IOException
   */
  public DariahSequenceTTLConstructor(RestHighLevelClient esClient, String inputLoglevel)
      throws IOException {
    super();
    this.esClient = esClient;

    log.info("creating DariahSequenceTTLConstructor Metadata Service");

    log.setLevel(Level.parse(inputLoglevel));
    log.info("setting loglevel to " + inputLoglevel);
  }

  /**
   * <p>
   * Creates the manifest's head from DH-crud metadata.
   * </p>
   */
  @Override
  public String getManifestHead(final String identifier, final String logID) throws IOException {

    String result = "";

    String handleWithoutPrefix = LTPUtils.omitHdlPrefix(identifier);

    log.fine("[" + logID + "] getting manifest head metadata for " + handleWithoutPrefix);

    // String resolvedHandleURL = LTPUtils.resolveHandlePid(identifier);

    // Get metadata for manifest head from DMD.
    List<String> titles = valueListFromES(handleWithoutPrefix, DMD, DC_TITLE, logID);
    String title = "";
    if (!titles.isEmpty()) {
      title = titles.get(0);
    }

    List<String> descriptions = valueListFromES(handleWithoutPrefix, DMD, DC_DESCRIPTION, logID);
    String description = "";
    if (!descriptions.isEmpty()) {
      description = descriptions.get(0);
    }

    List<String> attributions = valueListFromES(handleWithoutPrefix, DMD, DC_RIGHTS, logID);
    String licenses = "";
    for (String l : attributions) {
      try {
        URL u = new URL(l);
        licenses += "dcterms:license <" + u.toString() + ">;\n";
      } catch (MalformedURLException e) {
        licenses += "\tdcterms:license \"" + l + "\";\n";
      }
    }
    licenses = licenses.trim();

    // Set map for template TTL creation, get general vars.
    HashMap<String, Object> opts = getVars(identifier, "");
    // Set individual vars.
    opts.put(TITLE_FIELD, title);
    opts.put(DESCRIPTION_FIELD, description);
    opts.put(ATTRIBUTION_FIELD, StringUtils.arrayToDelimitedString(attributions.toArray(), "; "));
    opts.put(LICENSE_FIELD, licenses);
    opts.put(PRESENTATION_PATH, "/" + handleWithoutPrefix);

    result = SCUtils.createFromTemplate("manifest.tmpl", opts);

    return result;
  }

  /**
   * <p>
   * Creates the collection from metadata.
   * </p>
   */
  @Override
  public Collection getCollection(final String identifier, final String logID) throws IOException {

    // Create collection from ID.
    String handleWithoutPrefix = LTPUtils.omitHdlPrefix(identifier);
    Collection result = new Collection(
        this.serviceBaseUrl + "/" + identifier.replace("/", "%2F") + "/collection.json");
    result._context = Service.getJsonldContextUrl();

    log.fine("[" + logID + "] getting root collection metadata for " + handleWithoutPrefix);

    // String resolvedHandleURL = LTPUtils.resolveHandlePid(identifier);

    // Check collection type in ADMMD.
    if (!isCollection(handleWithoutPrefix, logID)) {
      String message = "[" + logID + "] no collection type! we need "
          + TextGridMimetypes.DARIAH_COLLECTION + "!";
      throw new IOException(message);
    }

    // Get metadata for collection manifest in DMD.
    List<String> collectionTitles = valueListFromES(handleWithoutPrefix, DMD, DC_TITLE, logID);
    for (String t : collectionTitles) {
      result.addLabel(t.trim());
    }

    List<String> collectionDescriptions =
        valueListFromES(handleWithoutPrefix, DMD, DC_DESCRIPTION, logID);
    for (String d : collectionDescriptions) {
      result.addDescription(d.trim());
    }

    List<String> collectionAttributions =
        valueListFromES(handleWithoutPrefix, DMD, DC_RIGHTS, logID);
    for (String a : collectionAttributions) {
      result.addAttribution(a.trim());
    }

    // Add Subcollections and Manifests.
    List<String> uriList = getUriList(handleWithoutPrefix, logID);

    // Create and add collections for every subcollection only.
    int count = 0;
    for (String id : uriList) {

      log.fine("[" + logID + "] check id for collection format: " + uriList);

      if (isCollection(id, logID)) {
        count++;

        log.fine("[" + logID + "] subcollection found");

        Collection c =
            new Collection(this.serviceBaseUrl + "/" + id.replace("/", "%2F") + "/collection.json");

        // Get and add titles as labels.
        List<String> subcollectionTitles = valueListFromES(id, DMD, DC_TITLE, logID);
        for (String t : subcollectionTitles) {
          c.addLabel(t.trim());
        }
        // Get and add licences as attributions.
        List<String> subcollectionAttributions = valueListFromES(id, DMD, DC_RIGHTS, logID);
        for (String a : subcollectionAttributions) {
          c.addAttribution(a.trim());
        }

        result.addCollection(c);

        log.fine("[" + logID + "] added subcollection " + count);
      }
    }

    // Add manifest for current collection.
    Manifest m =
        new Manifest(this.serviceBaseUrl + "/" + identifier.replace("/", "%2F") + "/manifest.json");
    m.setLabel(result.getLabel());
    m.setDescription(result.getDescription());
    m.setAttribution(result.getAttribution());
    result.addManifest(m);

    return result;
  }

  /**
   *
   */
  @Override
  public String getCanvas(final String collectionURI, final String itemURI, final String logID)
      throws IOException {

    String result = "";

    // Set map for template TTL creation, get general vars.
    HashMap<String, Object> opts = getVars(collectionURI, RDFConstants.HDL_PREFIX + ":" + itemURI);

    // Set label from title.
    // FIXME opts.put(result, opts);

    // Set individual vars.
    try {
      opts = setImageWidthAndHeightFromInfoJson(RDFConstants.HDL_PREFIX + ":" + itemURI, opts,
          this.iiifServiceUrlIntern, logID);
      result = SCUtils.createFromTemplate("canvas.tmpl", opts);

    } catch (ParseException | XMLStreamException e) {
      throw new IOException(e);
    }

    return result;
  }

  /**
   *
   */
  @Override
  public String getTitles(final String identifier, final List<String> uris, final String logID) {

    StringBuilder result = new StringBuilder();

    log.fine("[" + logID + "] getting titles for " + uris);

    try {
      for (String uri : uris) {
        List<String> titles = valueListFromES(uri, DMD, DC_TITLE, logID);
        String title = "";
        if (!titles.isEmpty()) {
          title = titles.get(0);
        }

        // Set map for template TTL creation, get general vars.
        HashMap<String, Object> opts = getVars(identifier, RDFConstants.HDL_PREFIX + ":" + uri);
        // Set individual vars.
        opts.put("title", title);

        result.append(SCUtils.createFromTemplate("label.tmpl", opts));
      }
    } catch (IOException e) {
      log.severe("[" + logID + "] error templating label.tmpl for " + identifier);
    }

    return result.toString();
  }

  /**
   *
   */
  @Override
  public String ttl(final String id, final String logID) throws MalformedURLException, IOException,
      TransformerException, ParseException, XMLStreamException {

    String result = "";

    String handleWithoutPrefix = LTPUtils.omitHdlPrefix(id);
    String pathFromURI = CachingUtils.getCachingPathFromURI(id);
    File f = new File(this.cacheDir.getAbsolutePath(), pathFromURI + MANIFEST_FILESUFFIX);

    if (ENABLE_CACHING && f.exists()) {
      log.info("[" + logID + "] using cached file " + f.getAbsolutePath());
      result = IOUtils.toString(new FileInputStream(f), Service.charset);
    } else {
      log.info("[" + logID + "] no cached file found, preparing " + f.getAbsolutePath()
          + " for creation");

      // Set map for template TTL creation, use general vars only.
      result = SCUtils.createFromTemplate("beginSequence.tmpl", getVars(id, ""));

      // Get overall metadata of every object contained in the collection.
      List<String> uriList = getUriList(handleWithoutPrefix, logID);

      // Check for images only.
      List<String> imageList = new ArrayList<String>();
      String canvasttl = "";
      if (!uriList.isEmpty()) {
        // Get canvas list and canvases.
        for (String uri : uriList) {
          if (uri != null && uri != "") {
            // Check for image formats.
            // TODO Care about deleted (and therefore not existing in ES) assets!
            String format = valueListFromES(uri, ADMMD, DCTERMS_FORMAT, logID).get(0);
            // TODO Is IMAGE_SET sufficient? Do we need a more lose image format check, such as
            // looking for "image" in the format string?
            if (TextGridMimetypes.IMAGE_SET.contains(format)) {
              imageList.add(uri);
              result += SCUtils.createFromTemplate("sequence.tmpl",
                  getVars(id, RDFConstants.HDL_PREFIX + ":" + uri));
              canvasttl += getCanvas(id, uri, logID) + "\n";
            }
          } else {
            log.warning("[" + logID + "] found null or empty uri in " + id + "!");
          }
        }
      }

      // End sequence.
      result += "\t) .\n";
      result += canvasttl;

      // Get titles.
      result += getTitles(id, imageList, logID);
    }

    return result;
  }

  /**
   * <p>
   * Gets a list of all contained objects.
   * </p>
   */
  @Override
  public List<String> getUriList(final String identifier, final String logID) throws IOException {

    List<String> result = new ArrayList<String>();

    initDHcrudClient(logID);

    try {
      // Get data from crud, check collection type, read file, if we have a DARIAH-DE collection.
      log.info("[" + logID + "] checking for collection type");

      if (isCollection(identifier, logID)) {
        log.info("[" + logID + "] got collection type, collecting URIs");

        Response res =
            DariahSequenceTTLConstructor.dhcrudClient.read(URI.create(identifier), 0, logID);

        String hdl = LTPUtils.resolveHandlePid(identifier);

        Model collection = RDFUtils.readModel(IOUtils.toString(res.readEntity(InputStream.class)),
            RDFConstants.TURTLE);

        List<String> objects = RDFUtils.getProperties(collection, hdl, RDFConstants.DCTERMS_PREFIX,
            RDFConstants.ELEM_DCTERMS_HASPART);

        // Omit all HDL namespaces.
        for (String url : objects) {
          result.add(LTPUtils.omitHdlNamespace(url));
        }

        log.info("[" + logID + "] collecting URIs complete");

      } else {
        log.warning("[" + logID + "] " + identifier + " is not a collection!");
      }
    } catch (XMLStreamException | org.apache.jena.riot.lang.extra.javacc.ParseException e) {
      throw new IOException(e);
    }

    return result;
  }

  /**
   * <p>
   * Get all DARIAH-DE repository collections from DH-rep OAI-PMH Data Provider.
   * </p>
   */
  @Override
  public Map<String, String> getCollections(final String[] listedProjects, final boolean sandbox,
      final String logID) {

    Map<String, String> result = new HashMap<String, String>();
    String listSetsRequest = this.oaipmhUrl + "?verb=ListSets";

    // Get collection and handle list from DH-rep OAI-PMH service (listSets).
    CloseableHttpClient client = HttpClientBuilder.create().build();
    try {
      HttpResponse response = client.execute(new HttpGet(listSetsRequest));

      // Check response.
      int statusCode = response.getStatusLine().getStatusCode();
      String reasonPhrase = response.getStatusLine().getReasonPhrase();
      if (statusCode == Status.OK.getStatusCode()) {
        String setsXML = IOUtils.toString(response.getEntity().getContent());
        result = SCUtils.getHandlesAndTitles(setsXML);
      } else {
        // Set some default values (that should never happen, in principle! :-)
        String message = "[" + logID + "] could not list sets from oaipmh service: "
            + statusCode + " " + reasonPhrase;
        log.severe(message);
      }
    } catch (IOException | ParserConfigurationException | SAXException e) {
      log.severe("[" + logID + "] " + e.getMessage());
    }

    return result;
  }

  /**
   *
   */
  @Override
  public String getAnnotation(final String identifier, final String name, final String logID)
      throws IOException {
    // TODO
    return null;
  }

  /**
   * @param json
   * @param key
   * @param theLogID
   * @return
   * @throws ParseException
   */
  protected static String parseInfoJSON(final String json, final String key, final String logID)
      throws ParseException {

    String result = null;

    // Get JSON object.
    JSONParser parser = new JSONParser();

    JSONObject jo = (JSONObject) parser.parse(json);
    if (jo != null && jo.containsKey(key)) {
      result = jo.get(key).toString();
    } else {
      String message = "[" + logID + "] error parsing key " + key + " from info.json";
      log.warning(message);
    }

    return result;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Create CRUD service client.
   * </p>
   */
  private void initDHcrudClient(final String logID) {
    if (dhcrudClient == null) {
      log.fine("[" + logID + "] " + CREATING_NEW + this.crudUrl);
      dhcrudClient = JAXRSClientFactory.create(this.crudUrl, DHCrudService.class);
    } else {
      log.fine("[" + logID + "] " + USING_EXISTING + this.crudUrl);
    }
  }

  /**
   * <p>
   * Checks for DARIAH-DE collection mimetype.
   * </p>
   * 
   * @param identifier
   * @param logID
   * @return
   * @throws IOException
   */
  private boolean isCollection(final String identifier, final String logID) throws IOException {

    boolean result = false;

    List<String> formatList = valueListFromES(identifier, ADMMD, DCTERMS_FORMAT, logID);

    if (formatList == null || formatList.isEmpty()) {
      String message = "[" + logID + "] no dcterms:format existing in ADMMD for " + identifier;
      log.severe(message);
      throw new IOException(message);
    } else {
      if (formatList.get(0).equals(TextGridMimetypes.DARIAH_COLLECTION)) {
        result = true;
      }
    }

    return result;
  }

  /**
   * <p>
   * Get width and height from info.json.
   * </p>
   * 
   * @param identifier
   * @param options
   * @param digilibServiceURL
   * @param logID
   * @return
   * @throws ParseException
   * @throws ClientProtocolException
   * @throws IOException
   * @throws XMLStreamException
   */
  private static HashMap<String, Object> setImageWidthAndHeightFromInfoJson(final String identifier,
      final HashMap<String, Object> options, final String digilibServiceURL, final String logID)
      throws ParseException, ClientProtocolException, IOException, XMLStreamException {

    // Set request path.
    String infoJSONRequest =
        digilibServiceURL + "/" + identifier.replaceAll("/", "%2F") + "/" + INFO_JSON;

    log.fine("[" + logID + "] info.json request: " + infoJSONRequest);

    CloseableHttpClient client = HttpClientBuilder.create().build();
    HttpResponse response = client.execute(new HttpGet(infoJSONRequest));

    // Check response.
    int statusCode = response.getStatusLine().getStatusCode();
    String reasonPhrase = response.getStatusLine().getReasonPhrase();
    String width = null;
    String height = null;
    if (statusCode == Status.OK.getStatusCode()) {
      // Process response.
      String json = IOUtils.readStringFromStream(response.getEntity().getContent());
      width = parseInfoJSON(json, HEIGHT, logID);
      height = parseInfoJSON(json, WIDTH, logID);
      if (width == null || height == null) {
        // Set some default values.
        String message = "[" + logID
            + "] got no image proportions from digilib server: setting default values ("
            + DEFAULT_IMAGE_X_SIZE + "/" + DEFAULT_IMAGE_Y_SIZE + ")";
        log.warning(message);
        // TODO Can we really set square settings here?
        width = DEFAULT_IMAGE_X_SIZE;
        height = DEFAULT_IMAGE_Y_SIZE;
      }
    } else {
      // Set some default values.
      String message = "[" + logID + "] could not get image proportions from digilib server: "
          + statusCode + " " + reasonPhrase + ", setting default values (" + DEFAULT_IMAGE_X_SIZE
          + "/" + DEFAULT_IMAGE_Y_SIZE + ")";
      log.warning(message);
      // TODO Can we really set square settings here?
      width = DEFAULT_IMAGE_X_SIZE;
      height = DEFAULT_IMAGE_Y_SIZE;
    }

    // Set width and height and return.
    options.put(HEIGHT, height);
    options.put(WIDTH, width);

    return options;
  }

  /**
   * <p>
   * Get as list of field values from ES.
   * </p>
   * 
   * @param identifier
   * @param fieldPath
   * @param field
   * @param logID
   * @return
   * @throws IOException
   */
  private List<String> valueListFromES(final String identifier, final String fieldPath,
      final String field, final String logID) throws IOException {

    List<String> result = new ArrayList<String>();

    String combinedField = fieldPath + "." + field;
    log.fine("[" + logID + "] get " + combinedField + " for " + identifier);

    // Set fields.
    String[] includes = new String[] {combinedField};
    String[] excludes = Strings.EMPTY_ARRAY;

    GetResponse fieldResponse =
        recordFromElasticSearch(identifier, includes, excludes, this.esIndex, this.esClient);

    // Only get values if maps are not null!
    Map<String, Object> fieldResponseMap = fieldResponse.getSourceAsMap();
    if (fieldResponseMap != null) {

      Map<String, Object> map = (Map<String, Object>) fieldResponseMap.get(fieldPath);
      if (map != null) {
        Object value = map.get(field);
        if (value instanceof String) {
          result.add((String) value);
        } else if (value instanceof List) {
          result.addAll((List<String>) value);
        }
      }
    }

    log.fine("[" + logID + "] field response list from es for " + identifier + ": " + result);

    return result;
  }

  /**
   * <p>
   * Get a response from ES.
   * </p>
   * 
   * @param identifier ID to get the ES record from.
   * @param includes Including the fields to fetch from the elastic search index.
   * @param excludes Excluding the fields to fetch from the elastic search index.
   * @param esindex
   * @param esclient
   * @return
   * @throws IOException
   */
  private static GetResponse recordFromElasticSearch(final String identifier,
      final String[] includes, final String[] excludes, final String esindex,
      final RestHighLevelClient esclient) throws IOException {

    // Setting the source context for fetching the fields from the elastic search index
    FetchSourceContext fetchSourceContext = new FetchSourceContext(true, includes, excludes);

    // Building the getRequest against the elastic search index
    GetRequest getRequest =
        new GetRequest(esindex, identifier).fetchSourceContext(fetchSourceContext);

    // Declaration of the result from the get-request
    GetResponse esResultObject = esclient.get(getRequest, RequestOptions.DEFAULT);

    return esResultObject;
  }

}
