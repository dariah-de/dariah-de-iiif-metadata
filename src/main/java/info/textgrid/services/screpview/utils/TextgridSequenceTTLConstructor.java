package info.textgrid.services.screpview.utils;

/*
 * #%L SharedCanvas Repository View
 *
 * %% Copyright (C) 2013 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.cxf.helpers.IOUtils;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.json.simple.parser.ParseException;
import org.springframework.util.StringUtils;
import de.digitalcollections.iiif.model.sharedcanvas.Collection;
import info.textgrid.middleware.common.CachingUtils;
import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.metadata.agent._2010.AgentType;
import info.textgrid.namespaces.metadata.agent._2010.PersonType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import info.textgrid.services.screpview.Service;
import info.textgrid.utils.sesameclient.SesameClient;

/**
 *
 */
public class TextgridSequenceTTLConstructor extends SequenceTTLConstructorAbs {

  protected static Logger log = Logger.getLogger(TextgridSequenceTTLConstructor.class.getName());

  private SesameClient sparqlClient;
  private SearchClient tgSearchClient;
  protected Transformer transformer;

  protected static final String URI_MAPPING_PREFIX =
      "PREFIX ore:<http://www.openarchives.org/ore/terms/>"
          + "PREFIX tg:<http://textgrid.info/relation-ns#>" + "SELECT ?uri ?pid WHERE {" + "<";
  protected static final String URI_MAPPING_SUFFIX = "> ^ore:aggregates* ?parent ."
      + "?parent ore:aggregates* ?uri ." + "?uri tg:hasPid ?pid ." + "}";
  protected static final String URI_FIELD = "textgridUri";

  /**
   * @param sparqlClient
   * @param tgSearchClient
   * @param esClient
   * @param metsXsltFile
   * @param inputLoglevel
   * @throws TransformerConfigurationException
   * @throws IOException
   */
  public TextgridSequenceTTLConstructor(SesameClient sparqlClient, SearchClient tgSearchClient,
      RestHighLevelClient esClient, String metsXsltFile, String inputLoglevel)
      throws TransformerConfigurationException, IOException {
    super();
    this.esClient = esClient;
    this.sparqlClient = sparqlClient;
    this.tgSearchClient = tgSearchClient;
    this.transformer = createTransformer(metsXsltFile, SCUtils.createRandomLogID());

    log.info("creating TextgridSequenceTTLConstructor Metadata Service");

    log.setLevel(Level.parse(inputLoglevel));
    log.info("setting loglevel to " + inputLoglevel);
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * info.textgrid.services.screpview.utils.SequenceTTLConstructor#getManifestHead(java.lang.String)
   */
  @Override
  public String getManifestHead(String identifier, String logID) throws IOException {

    // get license and rights holder from tgsearch
    String license = "";
    boolean rightsholder = false;
    String title = "";
    String description = "";
    List<String> attributions = new ArrayList<String>();

    String presentationPath = "browse/-/browse/" + identifier.substring(9).replace('.', '_');

    // TODO: sandbox
    Response ewmeta = this.tgSearchClient.getEditionWorkMeta(identifier);
    for (ResultType result : ewmeta.getResult()) {
      if ((result.getObject().getEdition() != null)
          && (result.getObject().getEdition().getLicense() != null)) {
        license = result.getObject().getEdition().getLicense().getLicenseUri();
      }
      if ((result.getObject().getItem() != null)
          && (result.getObject().getItem().getRightsHolder() != null)) {
        for (PersonType rightsholdertype : result.getObject().getItem().getRightsHolder()) {
          rightsholder = true;
          String rightsholderString = "rightsholder: " + rightsholdertype.getValue();
          if (!rightsholdertype.getId().isEmpty()) {
            rightsholderString += " [" + rightsholdertype.getId() + "]";
          }
          attributions.add(rightsholderString);
        }
      }
      if (result.getObject().getWork() != null) {
        if (result.getObject().getWork().getAbstract() != null) {
          description += StringUtils
              .arrayToDelimitedString(result.getObject().getWork().getAbstract().toArray(), " / ");
        }
        if (result.getObject().getWork().getAgent() != null) {
          for (AgentType agent : result.getObject().getWork().getAgent()) {
            String agentString = agent.getRole().value() + ": " + agent.getValue();
            if (!agent.getId().isEmpty()) {
              agentString += " [" + agent.getId() + "]";
            }
            attributions.add(agentString);
          }
        }
      }
      title = result.getObject().getGeneric().getProvided().getTitle().get(0);
      title = title.replaceAll("\"", "&#x22;");
    }

    String licensettl = "";
    if (!license.isEmpty()) {
      licensettl = "	 dcterms:license <" + license + ">;\n";
    }
    if (!rightsholder) {
      description += description.length() > 0 ? " / " : "";
      description +=
          "Unpublished testdata from the TextGrid repository sandbox. No attribution information available yet.";
    }

    // clear whitespaces and especially remove line breaks
    description = org.apache.commons.lang3.StringUtils.normalizeSpace(description);

    HashMap<String, Object> opts = getVars(identifier, "");
    opts.put(LICENSE_FIELD, licensettl);
    opts.put(ATTRIBUTION_FIELD, StringUtils.arrayToDelimitedString(attributions.toArray(), "; "));
    opts.put(TITLE_FIELD, title);
    opts.put(DESCRIPTION_FIELD, description);
    opts.put(PRESENTATION_PATH, presentationPath);

    return SCUtils.createFromTemplate("manifest.tmpl", opts);
  }

  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.services.screpview.utils.SequenceTTLConstructor#getCanvas(java.lang.String,
   * java.lang.String)
   */
  @Override
  public String getCanvas(String identifier, String name, String logID) throws IOException {

    String query = SCUtils.createFromTemplate("constructCanvas.tmpl", getVars(identifier, name));
    InputStream res = this.sparqlClient.constructTurtle(query);

    return IOUtils.toString(res, Service.charset);
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * info.textgrid.services.screpview.utils.SequenceTTLConstructor#getAnnotation(java.lang.String,
   * java.lang.String)
   */
  @Override
  public String getAnnotation(String identifier, String name, String logID) throws IOException {

    String query =
        SCUtils.createFromTemplate("constructAnnotation.tmpl", getVars(identifier, name));
    InputStream res = this.sparqlClient.constructTurtle(query);

    return IOUtils.toString(res, Service.charset);
  }

  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.services.screpview.utils.SequenceTTLConstructor#getTitles(java.lang.String,
   * java.util.List)
   */
  @Override
  public String getTitles(String id, List<String> uris, String logID) {

    StringBuilder sb = new StringBuilder();

    MultiGetRequest metadata4UrisRequest = new MultiGetRequest();
    for (String uri : uris) {
      // mets files may contain absolute URLs
      if (uri != null) {
        if (uri.startsWith(HTTP) || uri.startsWith(HTTPS)) {
          uri = uri.substring(uri.lastIndexOf("/") + 1);
        }

        String formattedId = uri.replaceAll(TEXTGRID_URI_PREFIX, "");

        ArrayList<String> fields = new ArrayList<String>();
        fields.add(URI_FIELD);
        fields.add(TITLE_FIELD);

        String[] excludes = Strings.EMPTY_ARRAY;

        FetchSourceContext fetchSourceContext =
            new FetchSourceContext(true, fields.toArray(new String[fields.size()]), excludes);

        metadata4UrisRequest.add(new MultiGetRequest.Item(this.esIndex, formattedId)
            .fetchSourceContext(fetchSourceContext));

      } else {
        log.warning("[" + logID + "] there was an empty uri getting titles for " + id
            + " in metadata uri request: " + uris.toString());
      }
    }

    MultiGetResponse metadata4allUris;
    try {
      metadata4allUris = this.esClient.mget(metadata4UrisRequest, RequestOptions.DEFAULT);
      for (MultiGetItemResponse metadata4allUrisResp : metadata4allUris.getResponses()) {

        if (metadata4allUrisResp.getResponse().isExists()) {
          Map<String, Object> metadata4allUrisContent =
              metadata4allUrisResp.getResponse().getSourceAsMap();
          if (metadata4allUrisContent == null) {
            log.severe("[" + logID + "] consistency problem: no content for id: "
                + metadata4allUrisResp.getId());
            continue;
          }

          Object uriField = metadata4allUrisContent.get(URI_FIELD);
          Object titleField = metadata4allUrisContent.get(TITLE_FIELD);
          String uri = uriField.toString();
          String title = titleField.toString();

          sb.append("<" + this.serviceBaseUrl + "/" + id + "/canvas/" + uri + ".json> rdfs:label \""
              + title + "\".\n");
        }
      }
    } catch (IOException e) {
      log.severe("[" + logID + "] error getting titles: " + e.getMessage());
    }

    return sb.toString();
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * info.textgrid.services.screpview.utils.SequenceTTLConstructor#getCollections(java.lang.String[]
   * , boolean)
   */
  @Override
  public Map<String, String> getCollections(String[] listedProjects, boolean sandbox,
      String logID) {

    Map<String, String> response = new HashMap<String, String>();

    // Create sandbox filter.
    BoolQueryBuilder sandboxFilter = QueryBuilders.boolQuery();
    if (!sandbox) {
      sandboxFilter.mustNot(QueryBuilders.existsQuery(NP_FIELD));
    } else {
      sandboxFilter.must(QueryBuilders.existsQuery(NP_FIELD));
    }

    // Add more filters.
    BoolQueryBuilder filter = QueryBuilders.boolQuery();
    filter.must(QueryBuilders.termQuery("format.untouched", "text/xml"))
        .must(QueryBuilders.termsQuery("project.id", listedProjects)).must(sandboxFilter);

    // Create query.
    // TODO Really use boolQuery here?
    QueryBuilder query =
        QueryBuilders.boolQuery().must(QueryBuilders.matchAllQuery()).filter(filter);

    // Set fields.
    String[] includeFields = new String[] {URI_FIELD, TITLE_FIELD, FORMAT_FIELD, NP_FIELD};
    String[] excludeFields = Strings.EMPTY_ARRAY;

    log.fine("incudeFields: " + includeFields.toString());
    log.fine("excudeFields: " + excludeFields.toString());

    // Create source builder.
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.fetchSource(includeFields, excludeFields).size(1000).query(query);

    // Create query here.
    SearchRequest esquery = new SearchRequest(this.esIndex).searchType(SearchType.QUERY_THEN_FETCH);
    esquery.source(searchSourceBuilder);

    log.fine("esquery: " + esquery);

    // Do search and get hits.
    log.fine("using elasticsearch client: " + this.esClient.toString());

    try {
      SearchResponse esresponse = this.esClient.search(esquery, RequestOptions.DEFAULT);

      // Loop and assemble response.
      SearchHits hits = esresponse.getHits();
      for (SearchHit hit : hits) {
        Map<String, Object> source = hit.getSourceAsMap();
        String title = source.get(TITLE_FIELD).toString();
        String tguri = source.get(URI_FIELD).toString();
        response.put(tguri, title);
      }
    } catch (IOException e) {
      log.severe("[" + logID + "] error getting collections: " + e.getMessage());
      e.printStackTrace();
    }

    return response;
  }

  /**
   *
   */
  @Override
  public Collection getCollection(String identifier, String logID) throws IOException {
    // TODO Auto-generated method stub
    return null;
  }

  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.services.screpview.utils.SequenceTTLConstructor#ttl(java.lang.String,
   * boolean)
   */
  @Override
  public String ttl(String id, String logID)
      throws MalformedURLException, IOException, TransformerException, ParseException {

    String sequencettl;

    String pathFromURI = CachingUtils.getCachingPathFromURI(id);
    File f = new File(this.cacheDir.getAbsolutePath(), pathFromURI + MANIFEST_FILESUFFIX);

    if (f.exists()) {
      sequencettl = IOUtils.toString(new FileInputStream(f), Service.charset);
    } else {
      log.info("[" + logID + "] creating ttl file for uri " + id + ": " + f.getCanonicalPath());

      List<String> uris = null;
      uris = getUriList(id, logID);

      if (uris == null) {
        String message = "[" + logID + "] error getting uri list for uri " + id;
        log.severe(message);
        throw new IOException(message);
      }

      sequencettl = "<" + this.serviceBaseUrl + "/" + id + "/sequence/normal.json"
          + "> a sc:Sequence;" + "rdfs:label \"Current Page Order\";" + " sc:hasCanvases (\n ";

      String canvasttl = "";
      for (String uri : uris) {
        if (uri != null && uri != "") {
          sequencettl += "    <" + this.serviceBaseUrl + "/" + id + "/canvas/" + uri + ".json> \n";
          canvasttl += getCanvas(id, uri, logID);
        } else {
          log.warning("[" + logID + "] there was an empty uri for " + id + " in uri list: "
              + uris.toString());
        }
      }
      sequencettl += ").";

      sequencettl += canvasttl;
      sequencettl += getTitles(id, uris, logID);
    }

    return sequencettl;
  }

  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.services.screpview.utils.SequenceTTLConstructor#getUriList(java.lang.String)
   */
  @Override
  public List<String> getUriList(String id, String logID)
      throws TransformerException, ParseException, UnsupportedEncodingException, IOException {

    List<String> uris = new ArrayList<String>();
    Map<String, String> pidmap = null;

    if (RESOLVE_PIDS) {
      pidmap = getPidUriMap(id);
    }

    String dataUrl = this.crudUrl + "/" + id + "/data";

    InputStream xmlStream = new URL(dataUrl).openStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    this.transformer.transform(new StreamSource(xmlStream), new StreamResult(out));
    ByteArrayInputStream uriStream = new ByteArrayInputStream(out.toByteArray());
    try (BufferedReader br = new BufferedReader(new InputStreamReader(uriStream))) {

      /**
       * handling of pids and absolute uris takes place here
       */
      String line;
      while ((line = br.readLine()) != null) {
        String uri = "";
        if (line.startsWith(TEXTGRID_URI_PREFIX)) {
          uri = line;
        } else if (RESOLVE_PIDS && (line.startsWith(HANDLE_PREFIX1)
            || line.startsWith(HANDLE_PREFIX2) || line.startsWith(HANDLE_PREFIX3))) {
          String handle = "";
          if (line.startsWith(HANDLE_PREFIX1)) {
            handle = URLDecoder.decode(line, "UTF-8").substring(HANDLE_PREFIX1.length());
          } else if (line.startsWith(HANDLE_PREFIX2)) {
            handle = URLDecoder.decode(line, "UTF-8").substring(HANDLE_PREFIX2.length());
          } else if (line.startsWith(HANDLE_PREFIX3)) {
            handle = URLDecoder.decode(line, "UTF-8");
          }
          uri = pidmap.get(handle);
        } else if ((line.startsWith(HTTP) || line.startsWith(HTTPS))
            && line.substring(line.lastIndexOf("/") + 1).startsWith(TEXTGRID_URI_PREFIX)) {
          uri = line.substring(line.lastIndexOf("/") + 1);
        } else {
          log.warning("[" + logID + "] ignoring not supported uri syntax: " + line.trim());
        }

        // add uri to list, if worthy :-)
        if (uri != null && !uri.trim().isEmpty() && !uri.trim().equals("unsupported")
            && !uris.contains(uri.trim())) {
          uris.add(uri.trim());
        }
      }
    }

    return uris;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param uri
   * @return
   * @throws IOException
   * @throws ParseException
   */
  private Map<String, String> getPidUriMap(String uri) throws IOException, ParseException {

    InputStream res = this.sparqlClient.sparqlJson(URI_MAPPING_PREFIX + uri + URI_MAPPING_SUFFIX);
    Map<String, String> resmap = SCUtils.mapFromSparqlJSON(res, "pid", "uri");

    return resmap;
  }

  /**
   * @param xsltFile
   * @return
   * @throws TransformerConfigurationException
   */
  private Transformer createTransformer(String xsltFile, String logID)
      throws TransformerConfigurationException {

    log.fine(
        "[" + logID + "] xsltloc: " + this.getClass().getResource("/xslt/" + xsltFile).toString());

    InputStream xsltStream =
        TextgridSequenceTTLConstructor.class.getResourceAsStream("/xslt/" + xsltFile);
    TransformerFactory factory = TransformerFactory.newInstance();
    Source xslt = new StreamSource(xsltStream);

    return factory.newTransformer(xslt);
  }

}
