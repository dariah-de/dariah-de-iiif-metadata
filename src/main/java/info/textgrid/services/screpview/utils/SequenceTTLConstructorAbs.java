package info.textgrid.services.screpview.utils;

/*
 * #%L SharedCanvas Repository View
 * 
 * %% Copyright (C) 2013 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen
 * (http://www.sub.uni-goettingen.de) %%
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import org.elasticsearch.client.RestHighLevelClient;

/**
 *
 */
public abstract class SequenceTTLConstructorAbs implements SequenceTTLConstructor {

  public static final boolean STORE_TTL_TO_DISK = true;
  protected static final boolean RESOLVE_PIDS = true;

  public static final String TTL_FILESUFFIX = ".ttl";
  public static final String MANIFEST_FILESUFFIX = ".manifest.json";
  public static final String COLLECTION_FILESUFFIX = ".collection.json";

  protected static final String TEXTGRID_URI_PREFIX = "textgrid:";
  protected static final String HANDLE_PREFIX1 = "http://hdl.handle.net/";
  protected static final String HANDLE_PREFIX2 = "https://hdl.handle.net/";
  protected static final String HANDLE_PREFIX3 = "hdl:";
  protected static final String USING_EXISTING = "using existing HTTP client from ";
  protected static final String CREATING_NEW = "creating new HTTP client from ";
  protected static final String HTTP = "http://";
  protected static final String HTTPS = "https://";
  protected static final String TITLE_FIELD = "title";
  protected static final String FORMAT_FIELD = "format";
  protected static final String DESCRIPTION_FIELD = "description";
  protected static final String ATTRIBUTION_FIELD = "attribution";
  protected static final String LICENSE_FIELD = "licensettl";
  protected static final String PRESENTATION_PATH = "presentationPath";

  protected static final String NP_FIELD = "nearlyPublished";

  protected RestHighLevelClient esClient;
  protected String esIndex;

  protected File cacheDir;
  protected String serviceBaseUrl;
  protected String crudUrl;
  protected String iiifServiceUrl;
  protected String iiifServiceUrlIntern;
  protected String oaipmhUrl;
  protected String iiifConformanceUrl;
  protected String repositoryUrl;
  protected String presentationUrl;
  protected String documentationUrl;
  protected String repositoryLogo;

  /**
   * 
   */
  public SequenceTTLConstructorAbs() {
    //
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param identifier
   * @param name
   * @return
   */
  protected HashMap<String, Object> getVars(String identifier, String name) {

    HashMap<String, Object> vars = new HashMap<String, Object>();
    vars.put("identifier", identifier.replaceAll("/", "%2F"));
    vars.put("name", name.replaceAll("/", "%2F"));
    vars.put("serviceBaseUrl", this.serviceBaseUrl);
    vars.put("crudUrl", this.crudUrl);
    vars.put("repositoryUrl", this.repositoryUrl);
    vars.put("presentationUrl", this.presentationUrl);
    vars.put("documentationUrl", this.documentationUrl);
    vars.put("repositoryLogo", this.repositoryLogo);
    vars.put("iiifServiceUrl", this.iiifServiceUrl);
    vars.put("iiifConformanceUrl", this.iiifConformanceUrl);
    vars.put("format", "\"image/jpeg\"");
    vars.put("oaipmhUrl", this.oaipmhUrl);

    return vars;
  }

  // **
  // GETTERS and SETTERS
  // **

  /**
   * @param esIndex
   */
  public void setEsIndex(String esIndex) {
    this.esIndex = esIndex;
  }

  /**
   * @return
   */
  public String getEsIndex() {
    return this.esIndex;
  }

  /**
   * @param esclient
   */
  public void setEsclient(RestHighLevelClient esclient) {
    this.esClient = esclient;
  }

  /**
   * @return
   */
  public RestHighLevelClient getEsClient() {
    return this.esClient;
  }

  /**
   * @return
   */
  @Override
  public String getServiceBaseUrl() {
    return this.serviceBaseUrl;
  }

  /**
   * @param serviceBaseUrl
   */
  @Override
  public void setServiceBaseUrl(String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  /**
   * @return
   */
  @Override
  public String getCrudUrl() {
    return this.crudUrl;
  }

  /**
   * @param tgcrudUrl
   */
  @Override
  public void setCrudUrl(String crudUrl) {
    this.crudUrl = crudUrl;
  }

  /**
   * @return
   */
  @Override
  public String getIiifServiceUrl() {
    return this.iiifServiceUrl;
  }

  /**
   * @param iiifServiceUrl
   */
  @Override
  public void setIiifServiceUrl(String iiifServiceUrl) {
    this.iiifServiceUrl = iiifServiceUrl;
  }

  /**
   * @return
   */
  @Override
  public String getIiifServiceUrlIntern() {
    return this.iiifServiceUrlIntern;
  }

  /**
   * @param iiifServiceUrlIntern
   */
  @Override
  public void setIiifServiceUrlIntern(String iiifServiceUrlIntern) {
    this.iiifServiceUrlIntern = iiifServiceUrlIntern;
  }

  /**
   * @return
   */
  @Override
  public String getIiifConformanceUrl() {
    return this.iiifConformanceUrl;
  }

  /**
   * @param iiifConformanceUrl
   */
  @Override
  public void setIiifConformanceUrl(String iiifConformanceUrl) {
    this.iiifConformanceUrl = iiifConformanceUrl;
  }

  /**
   * @param presentationUrl
   */
  public void setPresentationUrl(String presentationUrl) {
    this.presentationUrl = presentationUrl;
  }

  /**
   * @return
   */
  public String getPresentationUrl() {
    return this.presentationUrl;
  }

  /**
   * @param documentationUrl
   */
  public void setDocumentationUrl(String documentationUrl) {
    this.documentationUrl = documentationUrl;
  }

  /**
   * @return
   */
  public String getdocumentationUrl() {
    return this.documentationUrl;
  }

  /**
   * @return
   */
  @Override
  public String getRepositoryUrl() {
    return this.repositoryUrl;
  }

  /**
   * @param repositoryUrl
   */
  @Override
  public void setRepositoryUrl(String repositoryUrl) {
    this.repositoryUrl = repositoryUrl;
  }

  /**
   * @return
   */
  public String getOaipmhUrl() {
    return this.oaipmhUrl;
  }

  /**
   * @param logID
   */
  public void setOaipmhUrl(String oaipmhUrl) {
    this.oaipmhUrl = oaipmhUrl;
  }

  /**
   * @param repositoryLogo
   */
  public void setRepositoryLogo(String repositoryLogo) {
    this.repositoryLogo = repositoryLogo;
  }

  /**
   * @return
   */
  @Override
  public String getCacheDir() {
    return this.cacheDir.getAbsolutePath();
  }

  /**
   * @param cacheDir
   * @throws Exception
   * @throws IOException
   */
  public void setCacheDir(File cacheDir) throws IOException {
    this.cacheDir = cacheDir;
    if (!this.cacheDir.isDirectory() | !this.cacheDir.canWrite()) {
      throw new IOException(
          "cachedir not existing or not writeable: " + this.cacheDir.getCanonicalPath());
    }
  }

  /**
   * @param cachePath
   * @throws IOException
   * @throws Exception
   */
  public void setCacheDir(String cachePath) throws IOException, Exception {
    setCacheDir(new File(cachePath));
  }

}
