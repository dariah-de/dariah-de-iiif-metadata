package info.textgrid.services.screpview.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.StreamSupport;
import java.util.zip.Adler32;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.jsonldjava.core.JsonLdConsts;
import com.github.jsonldjava.core.JsonLdError;
import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import de.digitalcollections.iiif.model.sharedcanvas.Collection;
import info.textgrid.middleware.common.CachingUtils;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;

/**
 *
 */
public class SCUtils {

  // **
  // STATICS
  // **

  protected static Logger log = Logger.getLogger(SCUtils.class.getName());

  // **
  // FINALS
  // **

  public static final String ANNOTATION_FRAME_LOC = "/frame/annotation.json";
  public static final String MANIFEST_FRAME_LOC = "/frame/manifest2.json";
  public static final String SEQUENCE_FRAME_LOC = "/frame/sequence.json";
  public static final String CANVAS_FRAME_LOC = "/frame/canvas.json";
  public static final String TITLE = "title";
  public static final String HDL = "hdl";
  public static final String HDL_ENCODED = "hdl_encoded";
  public static final String HDL_DOUBLE_ENCODED = "hdl_double_encoded";
  public static final String SLASH_ENCODED = "%2F";
  public static final String SLASH_DOUBLE_ENCODED = "%252F";
  public static final String SET = "set";
  public static final String SET_NAME = "setName";
  public static final String SET_SPEC = "setSpec";
  public static final String LOG_ID_PREFIX = "IIIFMD_";

  /**
   * <p>
   * Converts Turtle to JsonLD, compacting and applying frame file if FrameLocation specified
   * </p>
   * 
   * @param dataset string containing turtle
   * @param contextUri location of jsonld context file, e.g.
   *        http://www.shared-canvas.org/ns/context.json
   * @param frameLocation one of SCUtils.ANNOTATION_FRAME_LOC, SCUtils.MANIFEST_FRAME_LOC,
   *        SCUtils.SEQUENCE_FRAME_LOC or SCUtils.CANVAS_FRAME_LOC, is loaded from resources
   * @return
   * @throws JSONLDProcessingError
   * @throws FileNotFoundException
   * @throws IOException
   * @throws JsonLdError
   */
  public static String ttl2jsonld(String dataset, URI contextUri, String imageServiceContextUri,
      String frameLocation) throws IOException, JsonLdError {

    JsonLdOptions inopts = new JsonLdOptions();

    inopts.format = JsonLdConsts.TEXT_TURTLE;
    inopts.setEmbed(true);
    inopts.setUseNativeTypes(true);

    JsonLdOptions outopts = new JsonLdOptions();

    log.fine("dataset: " + dataset);
    log.fine("contextUri: " + contextUri);
    log.fine("imageServiceContextUri: " + imageServiceContextUri);
    log.fine("frameLocation: " + frameLocation);

    // turtle to JSONLD
    Object jsonldobj = JsonLdProcessor.fromRDF(dataset, inopts);

    // apply framing if frame file specified
    Object frameobj;
    if (!frameLocation.equals("")) {
      InputStream framestream = SCUtils.class.getResourceAsStream(frameLocation);
      Object frame = JsonUtils.fromInputStream(framestream);
      frameobj = JsonLdProcessor.frame(jsonldobj, (Map<String, Object>) frame, outopts);
    } else {
      frameobj = jsonldobj;
    }

    // compact
    // Object contextobj = JsonUtils.fromURL(contextUri.toURL());
    // System.out.println(contextobj);

    Object compobj = JsonLdProcessor.compact(frameobj, contextUri.toString(), outopts);

    // get the contextUri back in
    LinkedHashMap outobj = new LinkedHashMap();
    outobj.put("@context", contextUri);
    outobj.putAll((LinkedHashMap) compobj);

    // Object compobj = frameobj;

    // put @context to every iiif image service reference
    JsonNode jn = new ObjectMapper().convertValue(outobj, JsonNode.class);
    StreamSupport.stream(jn.findValues("service").spliterator(), true)
        .filter(x -> x.getNodeType() == JsonNodeType.OBJECT)
        .forEach(x -> ((ObjectNode) x).put("@context", imageServiceContextUri));

    String jsonldString = JsonUtils.toPrettyString(jn)
        // somehow new jsonld-java does not remove prefix in key, if value is integer
        // https://github.com/jsonld-java/jsonld-java/issues/129
        // https://github.com/jsonld-java/jsonld-java/issues/132
        // so we do by hand here
        .replaceAll("exif:width", "width").replaceAll("exif:height", "height");

    return jsonldString;
  }

  /**
   * @param template
   * @param vars
   * @return
   * @throws IOException
   */
  public static String createFromTemplate(String template, HashMap<String, Object> vars)
      throws IOException {

    Writer writer = new StringWriter();
    MustacheFactory mf = new DefaultMustacheFactory();
    String templatePath =
        SCUtils.class.getClassLoader().getResource("templates/" + template).getPath();
    Mustache mustache = mf.compile(templatePath);
    mustache.execute(writer, vars);

    return writer.toString();
  }

  /**
   * @param in
   * @param var1
   * @param var2
   * @return
   * @throws IOException
   * @throws ParseException
   */
  public static Map<String, String> mapFromSparqlJSON(InputStream in, String var1, String var2)
      throws IOException, ParseException {

    Map<String, String> res = new HashMap<String, String>();

    InputStreamReader reader = new InputStreamReader(in);

    JSONParser parser = new JSONParser();
    Object obj = parser.parse(reader);
    JSONObject jsonObject = (JSONObject) obj;

    JSONObject jresults = (JSONObject) jsonObject.get("results");
    JSONArray jbindings = (JSONArray) jresults.get("bindings");

    for (Object val : jbindings) {
      JSONObject jval = (JSONObject) val;

      String val1 = (String) ((JSONObject) jval.get(var1)).get("value");
      String val2 = (String) ((JSONObject) jval.get(var2)).get("value");

      res.put(val1, val2);
    }

    return res;
  }

  /**
   * <p>
   * Processes a mustache template.
   * </p>
   * 
   * @param theTemplate
   * @param theVars
   * @return
   * @throws IOException
   */
  public static String processTemplate(String theTemplate, HashMap<String, Object> theVars) {

    String result = "";

    Writer writer = new StringWriter();
    MustacheFactory mf = new DefaultMustacheFactory();
    Mustache mustache = mf.compile(theTemplate);

    mustache.execute(writer, theVars);
    result = writer.toString();
    try {
      writer.close();
    } catch (IOException e) {
      // Don't know how this can happen. Just ignoring it! :-D
    }

    return result;
  }

  /**
   * <p>
   * Create a Set of HashMaps out of the given map to display just project titles and URLs.
   * </p>
   * 
   * @param theMap
   * @return
   */
  public static Set<Map<String, String>> makeMustacheMap4HTMLTemplate(Map<String, String> theMap) {

    Set<Map<String, String>> result = new HashSet<Map<String, String>>();

    for (String key : theMap.keySet()) {
      Map<String, String> m = new HashMap<String, String>();
      m.put(TITLE, theMap.get(key));
      m.put(HDL, LTPUtils.omitHdlPrefix(key));
      m.put(HDL_ENCODED, RDFConstants.HDL_PREFIX + ":"
          + LTPUtils.omitHdlPrefix(key.replaceAll("/", SLASH_ENCODED)));
      m.put(HDL_DOUBLE_ENCODED, RDFConstants.HDL_PREFIX + ":"
          + LTPUtils.omitHdlPrefix(key.replaceAll("/", SLASH_DOUBLE_ENCODED)));
      result.add(m);
    }

    return result;
  }

  /**
   * <p>
   * Creates a map of handles and titles from the DH-rep OAI-PMH ListSets response XML (key must be
   * the Handle!).
   * </p>
   * 
   * @param theListSetsXML
   * @return
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws SAXException
   */
  public static Map<String, String> getHandlesAndTitles(String theListSetsXML)
      throws ParserConfigurationException, SAXException, IOException {

    Map<String, String> result = new HashMap<String, String>();

    DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = fac.newDocumentBuilder();
    Document doc = builder.parse(new ByteArrayInputStream(theListSetsXML.getBytes()));

    NodeList setList = doc.getElementsByTagName(SET);
    for (int i = 0; i < setList.getLength(); i++) {
      Node set = setList.item(i);

      if (set.getNodeType() == Node.ELEMENT_NODE) {
        Element e = (Element) set;
        // We only can get title and HDL from listSets!
        String title = e.getElementsByTagName(SET_NAME).item(0).getTextContent();
        String hdl = e.getElementsByTagName(SET_SPEC).item(0).getTextContent();
        result.put(hdl, title);
      }
    }

    return result;
  }

  /**
   * <p>
   * Get JSON file path.
   * </p>
   * 
   * @return
   */
  public static String getJSONManifestPath(SequenceTTLConstructor theConstructor, String theID) {
    return theConstructor.getCacheDir() + File.separatorChar
        + CachingUtils.getCachingPathFromURI(theID) + SequenceTTLConstructorAbs.MANIFEST_FILESUFFIX;
  }

  /**
   * <p>
   * Get JSON file path.
   * </p>
   * 
   * @return
   */
  public static String getJSONCollectionPath(SequenceTTLConstructor theConstructor, String theID) {
    return theConstructor.getCacheDir() + File.separatorChar
        + CachingUtils.getCachingPathFromURI(theID)
        + SequenceTTLConstructorAbs.COLLECTION_FILESUFFIX;
  }

  /**
   * <p>
   * Creates the cache dir, if not yet existing.
   * </p>
   * 
   * @param theFile
   * @return The created cache dir.
   * @throws IOException
   */
  public static File createCacheDir(File theFile) throws IOException {

    File result = theFile.getParentFile();

    if (!result.exists()) {
      boolean doesItBlend = result.mkdirs();
      if (!doesItBlend) {
        String message = "Could not create cache dir " + result.getCanonicalPath();
        throw new IOException(message);
      }
    }

    return result;
  }

  /**
   * @param theString
   * @return
   * @throws JsonProcessingException
   */
  public static String getMappedString(Collection theCollection) throws JsonProcessingException {

    String result;

    ObjectMapper mapper = new ObjectMapper();
    mapper.enable(SerializationFeature.INDENT_OUTPUT);
    mapper.setSerializationInclusion(Include.NON_NULL);
    result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(theCollection);

    return result;
  }

  /**
   * <p>
   * Used for generating IDs for log messages.
   * </p>
   * 
   * @return
   */
  public static String createRandomLogID() {
    Adler32 adler = new Adler32();
    Random rn = new Random();
    adler.update(rn.nextInt());

    return LOG_ID_PREFIX + String.valueOf(adler.getValue());
  }

  /**
   * <p>
   * Transform %2F to / again for ES access, add "hdl:" prefix if not existing.
   * </p>
   * 
   * @param identifier
   * @return
   */
  public static String cleanIdentifier(final String identifier) {

    // Get / again.
    String result = identifier.replaceAll("%2F", "/");
    // add "hdl:" prefix.
    result = "hdl:" + LTPUtils.omitHdlPrefix(result);

    return result;
  }

  /**
   * <p>
   * Filter the enabled and broken projects out of the collections map.
   * </p>
   * 
   * @param theCollectionsMap
   * @param theProjectsToList
   * @param theBrokenURIs
   * @return
   */
  public static Map<String, String> filterCollections(Map<String, String> theCollectionsMap,
      String[] theProjectsToList, HashSet<String> theBrokenURIs) {

    Map<String, String> result = new HashMap<String, String>();

    log.fine("filter colections: " + theCollectionsMap.size());

    // If the listed projects list is not empty, show ONLY the listed projects!
    if (theProjectsToList != null && theProjectsToList.length != 0) {
      for (String p : theProjectsToList) {

        log.fine("add single project to list: " + p);

        result.put(p, theCollectionsMap.get(p));
      }
    }
    // Otherwise take all collections!
    else {

      log.fine("add all projects");

      result = theCollectionsMap;
    }

    // If the broken URI list is not empty, remove projects from list.
    if (theBrokenURIs != null && !theBrokenURIs.isEmpty()) {
      for (String p : theBrokenURIs) {

        log.fine("remove broken uri: " + p);

        result.remove(p);
      }
    }

    return result;
  }

}
