package info.textgrid.services.screpview.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 *
 */
@JsonInclude(Include.NON_EMPTY)
@JsonPropertyOrder({"@id", "@type", "location", "label"})
public class CollectionManifestEntry {

  private String id; // @id
  private String type = "sc:Manifest"; // @type
  private String label;

  /**
   * 
   */
  public CollectionManifestEntry() {}

  /**
   * @param id
   * @param label
   */
  public CollectionManifestEntry(String id, String label) {
    this.setId(id);
    this.setLabel(label);
  }

  /**
   * @return
   */
  @JsonProperty("@id")
  public String getId() {
    return this.id;
  }

  /**
   * @param id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return
   */
  public String getLabel() {
    return this.label;
  }

  /**
   * @param label
   */
  public void setLabel(String label) {
    this.label = label;
  }

  /**
   * @return
   */
  @JsonProperty("@type")
  public String getType() {
    return this.type;
  }

  /**
   * @param type
   */
  public void setType(String type) {
    this.type = type;
  }

}
