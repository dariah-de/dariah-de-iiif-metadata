package info.textgrid.services.screpview.json;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *
 */
@JsonInclude(Include.NON_EMPTY)
@JsonPropertyOrder({"@context", "@id", "@type", "label", "description", "attribution",
    "collections", "manifests"})
public class Collection {

  private String context = "http://iiif.io/api/presentation/2/context.json"; // @context
  private String id; // @id
  private String type = "sc:Collection"; // @type
  private String label;
  private String description;
  private String attribution;

  private List<Collection> collections = new ArrayList<Collection>();
  private List<CollectionManifestEntry> manifests = new ArrayList<CollectionManifestEntry>();

  /**
   * 
   */
  public Collection() {};

  // /**
  // * @param id
  // * @param label
  // * @param description
  // */
  // public Collection(String id, String label, String description) {}

  /**
   * @return
   */
  @JsonProperty("@context")
  public String getContext() {
    return this.context;
  }

  /**
   * @param context
   */
  public void setContext(String context) {
    this.context = context;
  }

  /**
   * @return
   */
  @JsonProperty("@type")
  public String getType() {
    return this.type;
  }

  /**
   * @param type
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return
   */
  @JsonProperty("@id")
  public String getId() {
    return this.id;
  }

  /**
   * @param id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return
   */
  public String getLabel() {
    return this.label;
  }

  /**
   * @param label
   */
  public void setLabel(String label) {
    this.label = label;
  }

  /**
   * @return
   */
  public String getDescription() {
    return this.description;
  }

  /**
   * @param description
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return
   */
  public String getAttribution() {
    return this.attribution;
  }

  /**
   * @param attribution
   */
  public void setAttribution(String attribution) {
    this.attribution = attribution;
  }

  /**
   * @return
   */
  public List<Collection> getCollections() {
    return this.collections;
  }

  /**
   * @param collections
   */
  public void setCollections(List<Collection> collections) {
    this.collections = collections;
  }

  /**
   * @param collection
   */
  public void addCollection(Collection collection) {
    this.collections.add(collection);
  }

  /**
   * @return
   */
  public List<CollectionManifestEntry> getManifests() {
    return this.manifests;
  }

  /**
   * @param manifest
   */
  public void setManifests(List<CollectionManifestEntry> manifest) {
    this.manifests = manifest;
  }

  /**
   * @param manifest
   */
  public void addManifest(CollectionManifestEntry manifest) {
    this.manifests.add(manifest);
  }

}
