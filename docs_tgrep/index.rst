.. IIIF documentation master file, created by
   sphinx-quickstart on Thu May 21 14:55:42 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


===================
IIIF Implementation
===================

The TextGrid Repository is able to deliver images and metadata according to the IIIF specifications. Namely this are the `IIIF Image API <http://iiif.io/api/image/1.1/>`_ and the `IIIF Presentation API <http://iiif.io/api/presentation/1.0>`_. This allows the usage of METS or TEI files stored in the TextGrid Repository with IIIF based tools like the Mirador Viewer. For more information on IIIF have a look at http://iiif.io/.


Poster/Presentation
-------------------

- IDCC, San Francisco 2014: `Image Services for Digital Archives <http://www.dcc.ac.uk/webfm_send/1466>`_

- DHd-Conference, Graz 2015: `Image Services und Annotationen mit IIIF, OpenAnnotation, Mirador, digilib und TextGrid <http://gams.uni-graz.at/o:dhd2015.v.003>`_

- IIIF-Conference, Vatican 2017: `TextGrid and Fontane <http://mathias-goebel.github.io/2017-06-IIIF/>`_


Enable IIIF for your Data
-------------------------

For now IIIF usage from `textgridrep.de <https://textgridrep.de>`_ is enabled on chosen projects, as it makes most sense for digitized objects like manuscripts or books, where scans of pages are available. The TextGrid IIIF compatibility layer reads the page order and links to images from DFG Viewer METS or TEI files. In TEI files the facs-attribute of the <pb> Element ist used to determine the images. If you publish data into the TextGrid Repository which matches this criteria of either being DFG Viewer METS or TEI files linking to digitized pages, drop a note to `textgrid-support@gwdg.de <mailto:textgrid-support@gwdg.de>`_, so we could activate IIIF and the link to the Mirador viewer for your project. After activation all METS/TEI Data published in this project will automatically use the IIIF feature.


Demonstration
-------------

There is some demonstration material published to the public TextGridRep Sandbox, to show the integration and usage of IIIF.

A listing of such objects is available here: https://textgridlab.org/1.0/iiif/manifests/


Mirador Viewer
--------------

TextGrid hosts an own version of the Mirador Viewer, that enables a few parameters to integrate published manifests or single items in the viewer. It is available via https://textgridlab.org/1.0/iiif/mirador/ and can parse the following parameter:

- uri
    - loads manifests to corresponding TextGrid URI
    - comma separated complete TextGrid URIs
    - when only on URI is present, the first canvasID will be loaded in ScrollView
    - **example**: https://textgridlab.org/1.0/iiif/mirador/?uri=textgrid:2smv7.0
- canvas
    - loads items into the viewer
    - format:
        - manifestURI_itemURI
        - comma separated
    - since the manifest URI is given here, the URI parameter is not needed more than one item requires layout parameter
    - **example**: https://textgridlab.org/1.0/iiif/mirador/?uri=textgrid:2smvp.0&canvas=textgrid:2smvp.0_textgrid:166j5.1
- layout
    - format: NxN
    - **examples**: https://textgridlab.org/1.0/iiif/mirador/?canvas=textgrid:2smvp.0_textgrid:166j5.1,textgrid:2smvp.0_textgrid:166kw.1&layout=1x2 and https://textgridlab.org/1.0/iiif/mirador/?canvas=textgrid:2smvp.0_textgrid:166j5.1,textgrid:2smvp.0_textgrid:166kw.1&layout=2x1
- tgrep
    - loads all manifests for data published in the TextGrid Repository
    - for more details see https://textgridlab.org/1.0/iiif/manifests/
- sandbox
    - loads all manifests for data (pre-) published in the TextGrid Repository Sandbox
    - for more details see https://textgridlab.org/1.0/iiif/manifests/


The source code is available at mirador_sources_.

For published IIIF enabled data within the TextGrid Repository website a link to Mirador is offered in the sidebar.


Technical Documentation
-----------------------

The IIIF implementation for TextGrid has two components, the TextGrid version of the `digilib <http://digilib.sourceforge.net/>`_ image server and a service to deliver the IIIF metadata.


Source Code
-----------

The GIT repositories are located at digilib_sources_ and iiif-metadata_sources_ (sharedcanvas)


Installation
------------

digilib and the sharedcanvas services both are built with maven. Building the .war files is done with

::

    mvn package


The war files from the /target/ subdirectory could afterwards be deployed in an application server like tomcat.


Configuration
-------------

The metadata service configuration is normally living in */etc/textgrid/screpview.properties* . This service is using spring, so advanced configuration could also happen in the *webapp/beans.xml* file, in this place also the path to the properties files could be changed.


Bugtracking
-----------

See iiif-metadata_bugtracking_


License
-------

See LICENCE_


.. _LICENCE: https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/-/blob/main/LICENSE.txt
.. _mirador_sources: https://gitlab.gwdg.de/dariah-de/mirador/-/tree/textgrid
.. _iiif-metadata_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata
.. _digilib_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/-/tree/main
.. _iiif-metadata_bugtracking: https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/-/issues
