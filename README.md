# The DARIAH-DE IIIF Metadata Services

## Overview

The TextGrid and DARIAH-DE Repositories deliver images and metadata according to the IIIF specifications. Namely this are the [IIIF Image API](http://iiif.io/api/image/1.1/) and the [IIIF Presentation API](http://iiif.io/api/presentation/1.0). This allows the usage of DARIAH-DE Collections stored in the DARIAH-DE Repository and the usage of METS or TEI files stored in the TextGrid Repository with IIIF based tools like IIIF Viewers. For more information on IIIF have a look at <http://iiif.io/>.

## Documentation

For service and API documentation please have a look at the [TextGrid Repository IIIF Documentation](https://textgridlab.org/doc/services/submodules/tg-iiif-metadata/docs_tgrep/index.html) and the [DARIAH-DE Repository IIIF Documentation](https://trep.de.dariah.eu/doc/services/submodules/dhiiifmd/docs_dhrep/index.html).

## Installation

Both services are provided with a Gitlab CI workflow to build Docker containers to be deployed on TextGrid and DARIAH-DE servers.

### Building from GIT using Maven

You can check out the IIIF Metadastas Services from our [GIT Repository](https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata), and then use Maven to build the IIIF Metadata Service WAR file:

    git clone https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata.git

...build the WAR package via:

    mvn clean package

You will get a IIIF Metadata Service WAR and/or DEB file in the folders ./target.

## Deploying the IIIF Metadata Service

The service is deployed just by installing the appropriate Docker container.

# Releasing a New Version

For releasing a new version of the DARIAH-DE IIIF Metadata Services, please have a look at the [DARIAH-DE Release Management Page](https://projects.academiccloud.de/projects/dariah-de-intern/wiki/dariah-de-release-management) and see the [Gitlab CI file](.gitlab-ci.yml).
