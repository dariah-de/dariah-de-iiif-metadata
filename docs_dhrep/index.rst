.. IIIF documentation master file, created by
   sphinx-quickstart on Thu May 21 14:55:42 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


===================
IIIF Implementation
===================

The DARIAH-DE Repository is able to deliver images and metadata according to the IIIF specifications. Namely this are the `IIIF Image API <http://iiif.io/api/image/1.1/>`_ and the `IIIF Presentation API <http://iiif.io/api/presentation/1.0>`_. This allows the usage of DARIAH-DE Collections stored in the DARIAH-DE Repository with IIIF based tools like the Mirador Viewer. For more information on IIIF have a look at http://iiif.io/.


Enable IIIF for your Data
-------------------------

++TODO++


Demonstration
-------------

++TODO++

A listing of objects is available here: https://repository.de.dariah.eu/1.0/iiif/manifests/


Mirador Viewer
--------------

DARIAH-DE hosts an own version of the Mirador Viewer, that enables a few parameters to integrate published manifests or single items in the viewer. It is available via https://textgridlab.org/1.0/iiif/mirador/ and can parse the following parameter:

- uri
    - loads manifests to corresponding DARIAH-DE Handle/DOI
    - comma separated complete DARIAH-DE Handle/DOI
    - when only on URI is present, the first canvasID will be loaded in ScrollView
    - **example**: ++TODO++
- canvas
    - loads items into the viewer
    - format:
        - manifestURI_itemURI
        - comma separated
    - since the manifest URI is given here, the URI parameter is not needed more than one item requires layout parameter
    - **example**: ++TODO++
- layout
    - format: NxN
    - **examples**: ++TODO++
- dhrep
    - loads all manifests for data published in the DARIAH-DE Repository
    - for more details see https://repository.de.dariah.eu/1.0/iiif/manifests/
- sandbox
    - loads all manifests for data (pre-) published in the TextGrid Repository Sandbox
    - for more details see https://repository.de.dariah.eu/1.0/iiif/manifests/


The source code is available at mirador_sources_.

For published IIIF enabled data within the DARIAH-DE Repository object's index pages a link to Mirador is offered in the footer.


Technical Documentation
-----------------------

The IIIF implementation for TextGrid has two components, the TextGrid version of the `digilib <http://digilib.sourceforge.net/>`_ image server and a service to deliver the IIIF metadata.


Source Code
-----------

The GIT repositories are located at digilib_sources_ and iiif-metadata_sources_ (sharedcanvas)


Installation
------------

digilib and the sharedcanvas services both are built with maven. Building the .war files is done with

::

    mvn package


The war files from the /target/ subdirectory could afterwards be deployed in an application server like tomcat.


Configuration
-------------

The metadata service configuration is normally living in */etc/textgrid/screpview.properties* . This service is using spring, so advanced configuration could also happen in the *webapp/beans.xml* file, in this place also the path to the properties files could be changed.


Bugtracking
-----------

See iiif-metadata_bugtracking_


License
-------

See LICENCE_


.. _LICENCE: https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/-/blob/main/LICENSE.txt
.. _mirador_sources: https://gitlab.gwdg.de/dariah-de/mirador/-/tree/textgrid
.. _iiif-metadata_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata
.. _digilib_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/-/tree/main
.. _iiif-metadata_bugtracking: https://gitlab.gwdg.de/dariah-de/dariah-de-iiif-metadata/-/issues
